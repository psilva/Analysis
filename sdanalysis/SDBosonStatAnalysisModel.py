from HiggsAnalysis.CombinedLimit.PhysicsModel import *
import itertools

class SDBosonModel(PhysicsModel):

    """ 
    Implements a probabilistic model to extract the fraction of SD events
    when the experiment counts in positive/negative rapidity bins
    in different vertex (or pileup) related categories
    A - asymmetry in SD events
    muSD/muNSD - are the scaling factors for the SD and NSD processes
    """

    def __init__(self):

        self.verbose = False
            
    def setPhysicsOptions(self,physOptions):

        """ parse physics options from command line (dummy for the moment) """

        for po in physOptions:
            if 'verbose=' in po:
                self.verbose = (po.replace('verbose=','') == 'True')
                print 'Verbose is {}'.format(self.verbose)

    def getYieldScale(self,bin,process):

        """ Decides which function to scale the process depending on the bin number and the process """

        if process=='SD' :        
            scale_func = '{}_func_{}_{}'.format(process,bin[0],bin[-3:])
            if self.verbose:
                print '[getYieldScale] for bin={} process={} => {}'.format(bin,process,scale_func)
        elif process=='NSD':
            scale_func='NSD_func'

        else:
            scale_func=1

        return scale_func

            
    def doParametersOfInterest(self):

        """Create POI and other parameters, and define the POI set."""

        #signal strength
        self.modelBuilder.doVar("muSD[1000,0.,15000.]")
        self.modelBuilder.doVar("muNSD[0.5,0.0,2.0]")

        #asymmetry in SD component
        self.modelBuilder.doVar("A[0.1]")

        #easier to write scaling of the yields with this expression
        self.modelBuilder.factory_("expr::r(\"(1-@0)/(1+@0)\",A)")

        #scaling functions
        if self.verbose:
            print 'doParametersOfInterest: instantiate scaling functions'

        self.modelBuilder.factory_('expr::NSD_func(\"@0\",muNSD)')

        for side,eta in itertools.product( ['n','p'],['neg','pos'] ):

            ibin='{}_{}'.format(side,eta)
            
            asymm_func='(@1/(1+@1))' if eta=='pos' else '(1./(1+@1))'
            sd_func  = 'expr::SD_func_{0}(\"@0*{1}\",muSD,r)'.format(ibin,asymm_func)
            self.modelBuilder.factory_(sd_func)

            if self.verbose:
                print '\t @ {} SD={}'.format(ibin,sd_func)

        pois='muSD,muNSD'
        self.modelBuilder.doSet("POI",pois)

        self.modelBuilder.out.Print()
        
sdboson = SDBosonModel()
