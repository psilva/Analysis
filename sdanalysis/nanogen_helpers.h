#ifndef __nanogen_helpers_h__ 
#define __nanogen_helpers_h__ 

#include "ROOT/RVec.hxx"
#include "Math/Vector4D.h"
#include <vector>

#include "Rivet/Analysis.hh"

using namespace ROOT::VecOps; 

using rvec_f = const RVec<float>;
using rvec_i = const RVec<int>;
using rvec_b = const RVec<bool>;


/**
    @short checks if it's of interest to keep at gen level: out-going proton or lepton
*/
rvec_b sdwGenLevel(const rvec_i &pdgId,const rvec_i &status, const rvec_i &statusFlags, const rvec_f &eta)
{
    std::vector<bool> keep(pdgId.size(),false);
    
    //loop over the first list of objects
    for(size_t i=0; i<pdgId.size(); i++) {
        if((abs(pdgId[i])>=11 || abs(pdgId[i])<=16) && status[i]==1 && (statusFlags[i] & 0x1)==1)
            keep[i]=true;        
    }

    return rvec_b(keep.begin(), keep.end());
}

/**
    @short returns true if it's an electron or muon
*/
rvec_b isChargedLepton(const rvec_i &pdgId)
{
    std::vector<bool> ischLepton(pdgId.size(),false);
    
    //loop over the first list of objects
    for(size_t i=0; i<pdgId.size(); i++) {
        ischLepton[i]=(Rivet::PID::isElectron(pdgId[i]) || Rivet::PID::isMuon(pdgId[i]));
    }

    return rvec_b(ischLepton.begin(), ischLepton.end());
}

/**
    @short returns true if it's charged
*/
rvec_b isCharged(const rvec_i &pdgId) {
    
    std::vector<bool> ischarged(pdgId.size(),false);
    
    //loop over the first list of objects
    for(size_t i=0; i<pdgId.size(); i++) {
        ischarged[i]=(Rivet::PID::charge(pdgId[i])!=0);
    }

    return rvec_b(ischarged.begin(), ischarged.end());
}

/**
    @short returns true if it's an electron or muon
*/
rvec_b isNeutrino(const rvec_i &pdgId)
{
    std::vector<bool> isNu(pdgId.size(),false);
    
    //loop over the first list of objects
    for(size_t i=0; i<pdgId.size(); i++) {
        isNu[i]=Rivet::PID::isNeutrino(pdgId[i]);
    }

    return rvec_b(isNu.begin(), isNu.end());
}

/**
    @short checks if a set of objects are isolated with respect to a reference in the eta-phi plane
*/
rvec_b crossClean(const rvec_f &eta,const rvec_f &phi, const float &eta_ref, const float &phi_ref,float cone=0.4)
{
    std::vector<bool> isIso;
    
    //loop over the first list of objects
    for(size_t i=0; i<eta.size(); i++) {
        
        //find the min. distance with respect to the second list of objects
        float minDR(9999.);
        isIso.push_back( DeltaR(eta[i],eta_ref,phi[i],phi_ref)>0.4 );
    }

    return rvec_b(isIso.begin(), isIso.end());
}

/**
    @short checks if a set of objects are isolated with respect to a reference in the eta-phi plane
*/
float largestRapidityGap(const rvec_f &jeta)
{
    //order etas by increasing value and return closer to the reference
    std::vector<float> alleta(jeta.begin(),jeta.end());
    if(alleta.size()==0) return 4.7;
    
    //if there is hadronic activity, sort by pseudo-rapidity
    std::sort(alleta.begin(), alleta.end() );
    float min_eta(alleta.front());
    float max_eta(alleta.back());
    float eta_boundary(min_eta);
    if(fabs(min_eta)>fabs(max_eta)) eta_boundary=max_eta;
    float lrg(4.7 - fabs(eta_boundary));
    float sgn_lrg=TMath::Sign(1,eta_boundary)*lrg;
    
    return sgn_lrg;
}

/**
    @short computes the contribution to the reconstruced xi (momentum loss) of a single object
*/
float xirec(const float &pt, const float &eta, const float &phi, const float &m, const float &p_sgn, float sqrts=13000.)
{
    if(m<1e-10) return 0.;
    ROOT::Math::PtEtaPhiMVector p4(pt,p_sgn*eta,phi,m);
    
    float mt(p4.Mt());
    float y(p4.Rapidity());
    if(mt<0 || TMath::IsNaN(y) || isinf(y)) {
        //std::cout << pt << "," << eta << ","  << phi <<","  << m << "\t -> ";
        //std::cout << mt << y << "from " << p4 << std::endl;
        return 0;
    }
    
    return (mt/sqrts)*TMath::Exp(y);
}


/**
    @short computes the reconstructed xi (momentum loss from a collection of objects)
*/
float xirecsum(const rvec_f &jpt, const rvec_f &jeta, const rvec_f &jphi, const rvec_f &jm, const rvec_b &jgood, const float &p_sgn, float sqrts=13000.)
{
    float xi(0.);
    if(jpt.size()==0) return xi;
    
    for(size_t i=0; i<=jpt.size(); i++) {
        if(!jgood[i]) continue;
        xi += xirec(jpt[i],jeta[i],jphi[i],jm[i],p_sgn,sqrts);
    }
    return xi;
}



/**
    @short checks if a set of objects are isolated with respect to a reference in the eta-phi plane
*/
rvec_b selPFCand(const rvec_i &gpf_g, const rvec_b &g_sel)
{
    std::vector<bool> isSel;
    for(size_t i=0; i<gpf_g.size(); i++)
        isSel.push_back( g_sel[gpf_g[i]] );

    return rvec_b(isSel.begin(), isSel.end());
}

#endif
