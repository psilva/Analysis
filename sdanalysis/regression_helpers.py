import os
import time
import torch 
import torch.nn as tnn 
import torch.nn.functional as F
from torch.utils.data import DataLoader,TensorDataset
from torch.optim.lr_scheduler import ExponentialLR
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mplhep as hep
plt.style.use([hep.style.CMS, hep.style.firamath])  
from scipy.optimize import curve_fit
from scipy.stats import norm



def prepareInputsForRegression(data,reg,ebeam=6500.):

    """builds the matrix of features and array of values to regress for the pz recoil"""
    
    #list of features
    featlist=['gl_pt','gl_eta','gen_hCMS_pt','gen_hCMS_dphi2l','gp_xi','gp_sgn']
    
    X = data[featlist].values
    
    #add the longitudinal component to the features
    pz_comps=['gp_pz','gl_pz']
    if reg=='TK'    : pz_comps += ['gen_hTK_pz']
    if reg=='CMS'   : pz_comps += ['gen_hTK_pz','gen_hHEF_pz']
    if reg=='CASTOR': pz_comps += ['gen_hTK_pz','gen_hHEF_pz','gen_hCASTOR_pz']
    if reg=='ZDC'   : pz_comps += ['gen_hTK_pz','gen_hHEF_pz','gen_hZDC_pz']
    hz = data[pz_comps].sum(axis=1).values
    X = np.c_[X,hz]
    
    #truth to regress
    y= -(data['gnu_pz']+hz).values/ebeam
    
    return X,y,featlist+pz_comps,len(pz_comps)


class SemiParametricBalanceModel(tnn.Module):
    
    def __init__(self, nfeat, nparamfeat, layers, dropout=0.5, activation=tnn.ReLU):
        """
        This class implements a semi-parametric regression ANN based on pytorch
        It is assumed that we are regressing a target where the final value is to be predicted  as a sum of features
        
        $target = \sum a_{i}(x) \tilde{x}_i$
    
        where x is a vector of features,  and $\tilde{x}$ is a subset of selected features 
        which are supposed to add up to the target.
        The network will make use of connected layers to predict the coefficients $a_{i}$ 
        and will return the estimate of the target      
        
        The input variables are the following:
        nfeat - number of features (x)
        nparamfeat - number of selected features of x 
        layers - array of number of neurons in the connected layers
        dropout - dropout fraction
        activation - activation function
        """
        
        #call parent __init__
        super().__init__()
        
        self.nfeat = nfeat
        self.nparamfeat = nparamfeat
        self.dropout = min(dropout,1)
        self.batchnorm = tnn.BatchNorm1d(self.nfeat)
                
        # define the layer structure
        layerlist = []
        for i,nout in enumerate(layers):
            nin=self.nfeat if i==0 else layers[i-1]
            layerlist.append(tnn.Linear(nin,nout))
            layerlist.append( activation(inplace=True) )
            layerlist.append(tnn.BatchNorm1d(nout))
            if self.dropout>0 : layerlist.append(tnn.Dropout(self.dropout))

        #last layer will produce the required outputs
        layerlist.append(tnn.Linear(layers[-1],nparamfeat+1))
        
        #list of layers is converted to a Sequential model as an attribute
        self.layers = tnn.Sequential(*layerlist)
        
        
    def forward(self, x):
        
        """
        performs the forward pass of the model
        """
        
        # extract the local features which will be summed up 
        # first column is the bias and it's filled with ones
        biasfeat=torch.ones(x.size(0)).reshape(-1,1)
        if torch.cuda.is_available():
            biasfeat=biasfeat.cuda()
        xparamfeat=torch.column_stack( (biasfeat,x[:,-self.nparamfeat:]))
        
        # normalize the incoming continuous data
        # evaluate the sequential part of the model
        # to predict the coefficients
        x = self.batchnorm(x)
        x = self.layers(x)
        
        #now use the predictions for the parameters of the functional form to compute the 
        #final
        delta=[]
        for i in range(self.nparamfeat+1):
            
            xi=x[:,i]
            xparamfeati=xparamfeat[:,i]
            delta.append( xi*xparamfeati) 

        delta = torch.stack(delta)
        delta = delta.sum(dim=0)
        return delta
        
    
    
def train_validate_SemiParametricBalanceModel(X,y,nparamfeat,
                                  fsplit=0.8,epochs=10,batch_size=1024,lr=1e-3,
                                  layers=[256,128,64,32],dropout=0.5,activation=tnn.ReLU,
                                  reportEvery=10,
                                  seed=42,
                                  root='./',
                                  tag=''):

    """
    train and validate the semiparametric model
    X,y - features and target
    nparamfeat - number of features for which the paramterization will be derived
    fsplit - splitting fraction for training / (1-fsplit) for test
    epochs - number of epochs to train
    batch - batch size
    lr - learning rate
    layers - number of nodes in the layers
    dropout - fraction to dropout
    activation - activation function
    seed - random number generator seed
    root - base directory where to store model and log file
    tag - a tag for this training
    """
    
    #try starting a torch tensor if type mismatches (force 32b float also)
    if type(X) != torch.Tensor:
        X=torch.Tensor(X.astype(np.float32))
        y=torch.tensor(y.astype(np.float32))
    
    #use a fixed seed for reproducibility
    torch.manual_seed(seed)
    
    #start the model
    model= SemiParametricBalanceModel(nfeat=X.shape[1],nparamfeat=nparamfeat,                              
                                      layers=layers,dropout=dropout,activation=activation)
    
    #if cuda is available turn to CUDA objects
    if torch.cuda.is_available():
        print("Using CUDA as it's available")
        X=X.cuda()
        y=y.cuda()
        model=model.cuda()
    else:
        print("Couldn't detect CUDA - you may want to connect to a host with available GPU")
        
    modelurl=f'{root}/semiparametric_model_{tag}.torch'
    
    #split into train, test
    fsplit = min(max(0.,fsplit),1.) #check range
    b = X.shape[0]        #full batch
    t = int((1-fsplit)*b) #test 
    train_tensors = TensorDataset(X[:b-t],y[:b-t])
    test_tensors = TensorDataset(X[b-t:b],y[b-t:b])    
    train_dataloader = DataLoader(train_tensors, batch_size=batch_size, shuffle=True)
    test_dataloader = DataLoader(test_tensors, batch_size=batch_size, shuffle=True)
    
    #start the optimizer
    optim_args={'params':model.parameters(),'lr':lr}
    optimizer = torch.optim.Adam(**optim_args)
    
    #scheduler to update the learning rate each epoch
    scheduler = ExponentialLR(optimizer, gamma=0.95)
    
    #loss function : this will be converted to RMSE later
    #criterion = tnn.SmoothL1Loss(beta=0.2)
    criterion = tnn.L1Loss()
    
    #prepare a file to store the train losses
    trainlogurl=f'{root}/semiparametric_train_{tag}.log'
    with open(trainlogurl,'w') as f:
        f.write('epoch\tbatch\ttrain\tloss\n')
    def _updateTrainLog(epoch,batch,istrain,loss):
        with open(trainlogurl,'a') as f:
            f.write(f'{epoch:d} {batch:d} {istrain} {loss:3.4f}\n')
            
    #start the training
    starttime = time.time()  
    ntrainbatches=int((b-t)/batch_size)
    nnoimprov=0
    min_reduced_test_loss=None
    for i in range(epochs):

        report = True if i%reportEvery==0 else False
        
        # Run the training batches
        batch_train_losses=[]
        for ibatch, (xtrain, ytrain) in enumerate(train_dataloader):
            
            # Apply the model
            ytrain = ytrain.reshape(-1,1)
            ypred = model(xtrain).reshape(-1,1)
            loss = criterion(ypred, ytrain)
            batch_train_losses.append( np.sqrt(loss.item()))
            _updateTrainLog(i,ibatch,True,batch_train_losses[-1])
            
            # Update parameters
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            
            #if report and ibatch%100==0:
            #    print(f'epoch {i} batch {ibatch}/{ntrainbatches} loss={batch_train_losses[-1]:.3f}')
                
        #update learning rate
        scheduler.step()
    
        #run the testing batches
        batch_test_losses = []
        with torch.no_grad():
             
            for b, (xtest, ytest) in enumerate(test_dataloader):

                # Apply the model
                ytest = ytest.reshape(-1,1)
                ypredtest = model(xtest).reshape(-1,1)

                # Update test loss & accuracy for the epoch
                loss = criterion(ypredtest, ytest)
                batch_test_losses.append(np.sqrt(loss.item()))
                _updateTrainLog(i,b,False,batch_test_losses[-1])
        
        #report status
        reduced_train_loss=np.mean(batch_train_losses)
        reduced_test_loss=np.mean(batch_test_losses)
        if report :
            print(f"epoch {i} average train/test loss is: {reduced_train_loss:.3f}/{reduced_test_loss:.3f}")
        if min_reduced_test_loss is None or reduced_test_loss<min_reduced_test_loss:
            min_reduced_test_loss=reduced_test_loss
            nnoimprov=0
            torch.save(model, modelurl)
        else:
            nnoimprov+=1
            if nnoimprov>40:
                print(f'average test loss did not improve after {nnoimprov} epochs=>stop training')
                break
            
    #log results location
    endtime = time.time()
    print(f'Best model stored @ {modelurl}')
    print(f'Test/train losses stored @ {trainlogurl}')
    print(f'Min average test loss achieved {min_reduced_test_loss:.3f}')
    print(f'Total elapsed time:{(endtime-starttime)/60:3.2f} min')
    
    #return best model url, and log url
    return modelurl,trainlogurl


def predict_SemiParametricBalanceModel(X,model):

    """
    runs the model predicition and updates the dataframe with the result
    X - array of features
    model - the torch model or the location of the torch file holding the model description
    """
    
    #eval in small batches
    if type(X) != torch.Tensor:
        X=torch.Tensor(X.astype(np.float32))
    if torch.cuda.is_available():
        X=X.cuda()
    eval_tensors = TensorDataset(X)
    eval_dataloader = DataLoader(eval_tensors, batch_size=1024, shuffle=False)
    
    #turn off dropout, batchnorm etc.
    if os.path.isfile(model):
        model=torch.load(model)
    model.eval()
    
    y=[]
    with torch.no_grad():
        for ibatch, xeval in enumerate(eval_dataloader):
            ypred=model.forward(xeval[0])
            if torch.cuda.is_available():
                ypred=ypred.cpu()            
            y.append( ypred.numpy() )
    y=np.concatenate(y)
    
    return y
        

def plot_loss(url,outf):
    
    """plots the evolution of the loss function"""
    
    #open log file with the loss function evolution
    df=pd.read_csv(url,sep=' ',skiprows=1,header=None,names=['epoch','batch','train','loss'])
    agg=df.groupby(by=['epoch','train']).agg( {'loss':['mean','std','min','max']} )
    agg.reset_index(inplace=True)
    
    #distinguish the train and test sets
    mask=(agg['train']==True)
    
    #overlay the loss function for the train and loss sets
    fig,ax=plt.subplots(figsize=(8,8))
    ax.fill_between(agg[mask]['epoch'],agg[mask][('loss','min')],agg[mask][('loss','max')],color='lightgray')
    ax.plot(agg[mask]['epoch'],agg[mask][('loss','mean')],color='gray',label='train')
    ax.errorbar(agg[~mask]['epoch'],agg[~mask][('loss','mean')],yerr=agg[~mask][('loss','std')],color='black',label='test',ls='',marker='o')
    ax.set_xlabel('Epoch')
    ax.set_ylabel('RMSE loss')
    ax.set_yscale('log')
    plt.legend()
    plt.grid()
    hep.cms.label(loc=0,data=False,ax=ax,rlabel='')
    fig.tight_layout()
    plt.savefig(outf)
    plt.close()
    
def plot_bias(X,y,y_pred,outdir,tag):
  
    delta = y_pred - y

    #differential bias
    fig,ax=plt.subplots(figsize=(8,8))
    ax.scatter(y,delta)
    ax.set_ylim(-2,1)
    ax.set_xlabel(r'$h^{z}_{undetected}$')
    ax.set_ylabel('Bias')
    fmt={'transform':ax.transAxes,'fontsize':16}
    ax.text(0.05,0.95,f'Regression model: {tag}',**fmt)
    ax.grid()
    hep.cms.label(loc=0)
    fig.tight_layout()
    outf=f'{outdir}/bias_differential_regression_{tag}.png'
    plt.savefig(outf)
    plt.close()
    
    #inclusive bias
    def _gauss(x, N, loc, scale):
        return N*norm.pdf(x,loc=loc,scale=scale)
        
    q=np.percentile(delta, [5,95])
   
    fig,ax=plt.subplots(figsize=(8,8))
    hist, bin_edges = np.histogram(delta,bins=np.linspace(-0.2,0.2,50))
    bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
    popt,pcov = curve_fit(_gauss, bin_centres, hist)
    fmt={'marker':'o','elinewidth':1,'capsize':1, 'ls':'none','c':'k'}
    plt.errorbar(bin_centres, hist, **fmt)
    plt.plot(bin_centres, _gauss(bin_centres, *popt),ls='--')
    plt.xlabel('Bias')
    plt.ylabel('Bias')
    fmt={'transform':ax.transAxes,'fontsize':16}
    plt.text(0.05,0.95,f'Regression model: {tag}',**fmt)
    plt.text(0.05,0.9,rf"$<bias>={popt[1]:3.4f}\pm{np.sqrt(pcov[1][1]):3.4f}$",**fmt)
    plt.text(0.05,0.85,rf"$\sigma_{{gauss}}={popt[2]:3.4f}\pm{np.sqrt(pcov[2][2]):3.4f}$",**fmt)
    plt.text(0.05,0.8,rf"$q_{{84}}-q_{{16}}={q[1]-q[0]:3.3f}$",**fmt)

    ax.set_yscale('log')
    ax.set_ylim(1,X.shape[0])
    ax.grid()
    hep.cms.label(loc=0,rlabel='')
    fig.tight_layout()
    outf=f'{outdir}/bias_regression_{tag}.png'
    plt.savefig(outf)
    plt.close()
