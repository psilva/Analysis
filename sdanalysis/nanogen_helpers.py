import ROOT
import itertools

def createGenLevelSnapshot(file_list,outname,debug=True, etamaxlist=[2.5,4.7,100]):
 
    """
    select the events and returns the result as a pandas DataFrame
    local: use multithread instead of spark
    debug: enable printouts
    """
    
    ROOT.ROOT.EnableImplicitMT()
     
    #include a header with helper functions (RIVET library is used)
    ROOT.gSystem.Load("$LCIO/lib/libRivet.so")
    ROOT.gROOT.ProcessLine(f'#include "nanogen_helpers.h"')
     
    #define the selections and branches with an RDataFrame
    base_rdf=ROOT.RDataFrame("Events", file_list)
            
    #charged lepton selection (trigger) - require one
    rdf = base_rdf.Define("gen_l",  'GenDressedLepton_pt>25 && abs(GenDressedLepton_eta)<2.5') \
             .Define('n_gl',   'ROOT::VecOps::Sum(gen_l)') \
             .Filter('n_gl==1') \
             .Define('gl_id',  'GenDressedLepton_pdgId[gen_l][0]') \
             .Define('gl_pt',  'GenDressedLepton_pt[gen_l][0]') \
             .Define('gl_phi', 'GenDressedLepton_phi[gen_l][0]') \
             .Define('gl_eta', 'GenDressedLepton_eta[gen_l][0]') \
             .Define('gl_m',   'abs(gl_id)==11 ? 0.0005110 : 0.10566') \
             .Define('gl_px',  'gl_pt*TMath::Cos(gl_phi)') \
             .Define('gl_py',  'gl_pt*TMath::Sin(gl_phi)') \
             .Define('gl_pz',  'gl_pt*TMath::SinH(gl_eta)' ) \
             .Define('gl_en',  'TMath::Sqrt(gl_m*gl_m+gl_pt*gl_pt+gl_pz*gl_pz)' )
      
    #gen particle level selection to identify the proton, the neutrino and the particles to estimate MET in CMS fiducial region
    rdf = rdf.Define('gen_p',      'GenPart_pdgId==2212 && GenPart_status==1 && GenPart_statusFlags==8193 && abs(GenPart_eta)>0') \
             .Define('gen_sel',    'sdwGenLevel(GenPart_pdgId,GenPart_status,GenPart_statusFlags,GenPart_eta)') \
             .Define('gen_nu',     'gen_sel && isNeutrino(GenPart_pdgId)') \
             .Define('gen_met',    'GenPart_status==1 && !isNeutrino(GenPart_pdgId)') \
             .Define('GenPart_px', 'GenPart_pt*ROOT::VecOps::cos(GenPart_phi)' ) \
             .Define('GenPart_py', 'GenPart_pt*ROOT::VecOps::sin(GenPart_phi)' ) \
             .Define('GenPart_pz', 'GenPart_pt*ROOT::VecOps::sinh(GenPart_eta)' ) \
             .Define('GenPart_en', 'ROOT::VecOps::sqrt(GenPart_mass*GenPart_mass+GenPart_pt*GenPart_pt+GenPart_pz*GenPart_pz)' )        
    for p in ['p','nu']:
        rdf = rdf.Define(f'n_g{p}',   f'ROOT::VecOps::Sum(gen_{p})') \
                 .Define(f'g{p}_px',  f'ROOT::VecOps::Sum(GenPart_px[gen_{p}])') \
                 .Define(f'g{p}_py',  f'ROOT::VecOps::Sum(GenPart_py[gen_{p}])') \
                 .Define(f'g{p}_pz',  f'ROOT::VecOps::Sum(GenPart_pz[gen_{p}])') \
                 .Define(f'g{p}_en',  f'ROOT::VecOps::Sum(GenPart_en[gen_{p}])') \
                 .Define(f'g{p}_pt',  'TMath::Sqrt(g{0}_px*g{0}_px+g{0}_py*g{0}_py)'.format(p)) \
                 .Define(f'g{p}_phi', 'TMath::ATan2(g{0}_py,g{0}_px)'.format(p)) \
                 .Define(f'g{p}_eta', 'g{0}_pt!=0 ? TMath::ASinH(g{0}_pz/g{0}_pt) : -9999.'.format(p)) 
            
    #additional quantities
    for p in ['p','nu','l']:
        rdf=rdf.Define(f'g{p}_xi',  f'1.-TMath::Abs(g{p}_pz)/6500.') \
               .Define(f'g{p}_sgn', f'TMath::Sign(1,g{p}_eta)')
        
    #define proton in acceptance (PPS cuts)
    rdf=rdf.Define('gpinacc', 'gp_sgn>0 ? (gp_xi>0.025 && gp_xi<0.15) : (gp_xi>0.04 && gp_xi<0.18)')

    #momentum balance of all particles except the lepton, neutrino and proton
    for reg in ['TK','HEF','CMS','CASTOR','ZDC','IDEAL']:
        
        selcuts=f'gen_met && crossClean(GenPart_eta,GenPart_phi,gp_eta,gp_phi) && crossClean(GenPart_eta,GenPart_phi,gl_eta,gl_phi) && crossClean(GenPart_eta,GenPart_phi,gnu_eta,gnu_phi)'
        if reg=='TK':
            selcuts += f' && GenPart_pt>0.1 && abs(GenPart_eta)<2.5'
        if reg=='HEF':
            selcuts += f' && GenPart_pt>0.1 && abs(GenPart_eta)>=2.5 && abs(GenPart_eta)<4.7'
        if reg=='CMS':
            selcuts += f' && GenPart_pt>0.1 && abs(GenPart_eta)<4.7'
        if reg=='CASTOR':
            selcuts += f' && abs(GenPart_eta)>=5.2 && abs(GenPart_eta)<6.6'
        if reg=='ZDC':
            selcuts += f' && abs(GenPart_eta)>=8.5'
                
        rdf = rdf.Define(f'good_gen_h{reg}',selcuts) \
                 .Define(f'gen_h{reg}_px',     f'Sum(GenPart_px[good_gen_h{reg}])') \
                 .Define(f'gen_h{reg}_py',     f'Sum(GenPart_py[good_gen_h{reg}])') \
                 .Define(f'gen_h{reg}_pz',     f'Sum(GenPart_pz[good_gen_h{reg}])') \
                 .Define(f'gen_h{reg}_pt',     'TMath::Sqrt(gen_h{0}_px*gen_h{0}_px+gen_h{0}_py*gen_h{0}_py)'.format(reg)) \
                 .Define(f'gen_h{reg}_phi',    f'TMath::ATan2(gen_h{reg}_py,gen_h{reg}_px)') \
                 .Define(f'gen_h{reg}_dphi2l', f'TVector2::Phi_mpi_pi(gl_phi-gen_h{reg}_phi)')
                    
    #save snapshot with interesting branches
    branch_names_list = ['Generator_weight','event', 'n_gp', 'n_gl', 'n_gnu', 'gpinacc','gl_id']
    for p,v in itertools.product(['p','nu','l'],['pt','eta','phi','en','px','py','pz','sgn','xi']):
        branch_names_list.append( f'g{p}_{v}')        
    for reg,v in itertools.product(['TK','HEF','CMS','CASTOR','ZDC','IDEAL'],['px','py','pz','pt','phi','dphi2l']):
        branch_names_list.append( f'gen_h{reg}_{v}' )    
    branchList = ROOT.vector('string')()
    for b in branch_names_list: branchList.push_back(b)
    rdf.Snapshot('Events', outname, branchList)
    
    #compute also weight sum and save as simple counting histogram in the file
    totalWeights = base_rdf.Sum('Generator_weight').GetValue()
    h=ROOT.TH1F('wsum','wsum',1,0,1)
    h.SetBinContent(1,totalWeights)
    fOut=ROOT.TFile.Open(outname,'UPDATE')
    h.SetDirectory(fOut)
    h.Write()
    fOut.Close()
    
    #disable the implicit multi-threading
    ROOT.ROOT.DisableImplicitMT()
        
    return totalWeights