#!/bin/bash

#parse input to analyze
npv=${1}
if [ -z ${npv} ]; then
   npv="npv"
fi
ch=${2}
if [ -z ${ch} ]; then
   ch="mu"
fi
outdir=${ch}_${npv}_stat
mkdir -p ${outdir}
echo "Starting statistical analysis for npv=${npv} with output @ ${outdir}"

# combine the datacards
base_card=/eos/user/p/psilva/www/sdanalysis/2021Oct15/${ch}
card_str=""
for i in 1 2; do
    for s in n p; do
        card_str="${card_str} ${s}_${i}=${base_card}_${s}_${npv}${i}.dat"
     done
done

if [ "${npv}" == "npvgood" -a "${ch}" == "mu" ]; then
    for s in n p; do
        card_str="${card_str} ${s}_0=${base_card}_${s}_${npv}0.dat"
     done
fi
combineCards.py ${card_str} > ${base_card}_combined.dat
cp ${base_card}_combined.dat ${outdir}/datacard.dat
echo "Datacards combined: ${card_str}"
echo "Combined datacard in ${outdir}/datacard.dat"

text2workspace.py ${outdir}/datacard.dat -o ${outdir}/workspace.root -P HiggsAnalysis.CombinedLimit.SDBosonStatAnalysisModel:sdboson --PO verbose=True
echo "Workspace available at ${outdir}/workspace.root"

#postfit nuisances
echo "Running FitDiagnostics"
combine -M FitDiagnostics ${outdir}/workspace.root --saveShapes --saveWithUncertainties --cminDefaultMinimizerStrategy 0
python test/diffNuisances.py ${outdir}/fitDiagnosticsTest.root --poi muSD -g ${outdir}/nuisances.root

#likelihood scans
echo "Running likelihood scans"
combine -M MultiDimFit ${outdir}/workspace.root --algo grid --points 100 -P muSD  --fastScan --floatOtherPOIs=1 -n muSD  --setParameterRanges muSD=0,5000
combine -M MultiDimFit ${outdir}/workspace.root --algo grid --points 100 -P muNSD --fastScan --floatOtherPOIs=1 -n muNSD
combine -M MultiDimFit ${outdir}/workspace.root --algo grid --points 100 -P A     --fastScan --floatOtherPOIs=1 -n A     --setParameterRanges A=0,0.5

mv fitDiagnostics*.root ${outdir}
mv higgsCombine*.root ${outdir}
