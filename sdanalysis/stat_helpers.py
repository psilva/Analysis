from scipy.stats import gamma 
import numpy as np

def countInHistogram(x,w,bins):
    
    """
    returns the bins, the counts per bins and the associated errors
    in case of unweighted data, the errors
    """
    if w is None:
        n, bins = np.histogram(x, bins=bins)
        #cf. (Asymmetric Error Bars for Poisson Event 
        #Counts)[https://twiki.cern.ch/twiki/bin/viewauth/CMS/PoissonErrorBars]
        alpha = 1 - 0.6827
        L = np.where(n==0,0.,gamma.ppf(alpha/2,n))
        U = gamma.ppf(1-alpha/2,n+1)
        n_err=np.stack( [n-L,U-n], axis=1).T
    else:
        n, bins = np.histogram(x, bins=bins, weights=w)
        n_err = np.sqrt(np.histogram(x, bins=bins, weights=w**2)[0])
    return [n,n_err]


def showComparisonPlot(stack=None, stack_labels=None,
                       data=None, data_label=None,
                       overlay=None, overlay_labels=None,
                       overlay_bottom=None, overlay_bottom_labels=None, simpleOverlay=True,
                       logy=False, logx=False, grid=True, scaleToData=False,
                       xlabel='x', ylabel='y', rylabel='Obs. / Pred.', ptitle=None,
                       yran=None, ryran=None, handle=None, xran=None,
                       legendFontSize=16,
                       outname=None):
    
    """
    classical data/MC stacked plot with a ratio panel
    stack and overlay are lists of (bins,counts,errors)
    data is a tuple of (bins,counts,(2,N) errors)
    the remaining parameters are self-explanatory
    based on this [example](https://github.com/nsmith-/mpl-hep/blob/master/binder/gallery.ipynb)
    if outname is given the plot is stored in .png/.pdf formats
    """
    
    kwargs={'figsize': (10, 8), 'gridspec_kw': {'height_ratios': (3, 1)},}
    fig, (ax, rax) = plt.subplots(2, 1, sharex=True, **kwargs)
    fig.subplots_adjust(hspace=.07)  # this controls the margin between the two axes

    ebar_style={'marker':'o','elinewidth':1,'capsize':1,'color':'k','ls':'none'}
    fill_style={'step':'post','facecolor':'lightgray','alpha':0.5,'edgecolor':'black', 'linewidth':0, 'hatch':'///'}
    overlay_style={'ls':'-','drawstyle':'steps','lw':2}
    overlay_ebar_style={'marker':'o','elinewidth':1,'capsize':1,'ls':'none'}

    #show the stack of histograms
    if stack:
    
        bins=stack[0][0]
        bin_centers=0.5*(bins[0:-1]+bins[1:])

        #stack the histograms
        sumw_stack = np.vstack([h[1] for h in  stack])
        sumw_stack = np.hstack([sumw_stack, sumw_stack[:,-1:]])  
        sumw_total = sumw_stack.sum(axis=0)
        if scaleToData:
            print('Not yet implemented')
            pass
        ax.stackplot(bins, sumw_stack, labels=stack_labels, step='post', edgecolor=(0, 0, 0, 0.5),)

        # Overlay an uncertainty hatch        
        sumw_unc = np.vstack([h[2] for h in  stack])
        sumw_unc = np.sqrt(np.sum(np.square(sumw_unc), axis=0))
        sumw_unc = np.hstack([sumw_unc, sumw_unc[-1]])
        ax.fill_between(x=bins, 
                        y1=sumw_total-sumw_unc, 
                        y2=sumw_total+sumw_unc,
                        label='Unc.', 
                        **fill_style
                       )
    
    #overlay
    if overlay:        
        for i in range(len(overlay)):   
            b,h,hunc=overlay[i]
            label=overlay_labels[i]
            ax.plot(b, np.insert(h, 0, h[0]), label=label, **overlay_style)

    # Draw the observation
    if data:
        bins=data[0]
        bin_centers=0.5*(bins[0:-1]+bins[1:])
        yerr=data[2]
        if yerr.shape[0]>2 : yerr=yerr.T
        ax.errorbar(x=bin_centers, 
                    y=data[1], 
                    yerr=yerr,
                    label=data_label, 
                    **ebar_style)
 
    #tweak the final formatting
    if yran:
        ax.set_ylim(*yran)
    else:
        ax.set_ylim(0, None)
    if xran:
        ax.set_xlim(*xran)
    ax.set_ylabel(ylabel)
    if logy: ax.set_yscale('log')
    if logx: ax.set_xscale('log')
    if grid: ax.grid()
    ax.legend(fontsize=legendFontSize)
    if handle: handle(ax)

    # Draw a comparison panel
    if stack: 
        # separate rather than combining (as per tradition)
        rax.fill_between(x=bins, 
                         y1=1 - sumw_unc/sumw_total, 
                         y2=1 + sumw_unc/sumw_total, 
                         **fill_style)
        
        #draw ratio
        if data:
            sumw_total = sumw_total[:-1]  # Take away that copy of the last bin
            rax.errorbar(x=bin_centers, y=data[1]/sumw_total, yerr=data[2].T/sumw_total, **ebar_style)

    #if also passed draw these
    if overlay_bottom:
        for i in range(len(overlay_bottom)):   
            b,h,hunc=overlay_bottom[i]
            label=overlay_bottom_labels[i] if overlay_bottom_labels else None
            if simpleOverlay:
                rax.plot(b, np.insert(h, 0, h[0]), label=label, **overlay_style)
            else:
                bin_centers=0.5*(b[0:-1]+b[1:])
                rax.errorbar(x=bin_centers, y=h, yerr=hunc, label=label, **overlay_ebar_style)
        if overlay_bottom_labels: rax.legend()
            
    if ryran:
        rax.set_ylim(*ryran)
    else:
        rax.set_ylim(0.45, 1.55)
    if xran:
        rax.set_xlim(*xran)
    rax.set_ylabel(rylabel)
    if logx: rax.set_xscale('log')
    if grid: rax.grid()
    rax.set_xlabel(xlabel)
    rax.autoscale(axis='x', tight=True) 
    
    if ptitle:
        hep.cms.label(loc=0,data=True,ax=ax,rlabel=ptitle)
    else:
        hep.cms.label(loc=0,data=True,ax=ax)
    fig.tight_layout()
            
    if outname:
        for ext in ['png','pdf']:
            plt.savefig('{}.{}'.format(outname,ext))
        plt.close()
        print('Plot saved in {}.{{png,pdf}}'.format(outname))
