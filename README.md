# Data analysis codes

This repository holds ad-hoc codes used in different data analysis codes in CMS

Most are implemented as notebooks which can be run using SWAN
To get started you can either click here:

[![SWAN](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://cern.ch/swanserver/cgi-bin/go/?projurl=https://gitlab.cern.ch/psilva/analysis.git)

for an automated start, or do it by hand with the following steps

1. go to swan.cern 
1. open a terminal in the web page
1. clone this repository `git clone https://gitlab.cern.ch/psilva/analysis.git`
1. open the analysis you want to execute
