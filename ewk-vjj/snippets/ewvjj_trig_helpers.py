import copy 
import numpy as np
from statsmodels.stats.proportion import proportion_confint

var_dict={'pt':'Photon transverse momentum [GeV]',
          'ptj1':'Leading jet transverse momentum [GeV]',
          'ptj2':'Sub-leading jet transverse momentum [GeV]',
          'maj1':'Mass($\gamma$,$j_1$) [GeV]',
          'maj2':'Mass($\gamma$,$j_2$) [GeV]',
          'eta':'Photon pseudo-rapidity',
          'PV_npvsGood':'Primary vertex multiplicity',
          'mjj':'Dijet invariant mass',
          'absdetajj':r"$|\Delta\eta$(j,j')|"} 

mask_cuts={ 'hpt_base'     : {'pt':200},
            'hpt_pt'       : {'pt':150},
            'lpt_base'     : {'pt':75,'isScEtaEB':True,'mjj':500,'detajj':3.0,'ptj1':40,'ptj2':40},
            'lpt_basenovbf': {'pt':95,'isScEtaEB':True},
            'lpt_ptj1'     : {'pt':75,'isScEtaEB':True,'mjj':500,'detajj':3.0,'ptj1':30,'ptj2':40},
            'lpt_ptj2'     : {'pt':75,'isScEtaEB':True,'mjj':500,'detajj':3.0,'ptj1':40,'ptj2':30},
            'lpt_pt'       : {'pt':50,'isScEtaEB':True,'mjj':500,'detajj':3.0,'ptj1':40,'ptj2':40},
            'lpt_ptnovbf'  : {'pt':75,'isScEtaEB':True},
            'lpt_pt_mjj'   : {'pt':50,'isScEtaEB':True,'mjj':300,'detajj':3.0,'ptj1':40,'ptj2':40},
            'lpt_mjj_ptj2' : {'pt':75,'isScEtaEB':True,'mjj':300,'detajj':3.0,'ptj1':40,'ptj2':40},
            'lpt_mjj'      : {'pt':75,'isScEtaEB':True,'mjj':300,'detajj':3.0,'ptj1':40,'ptj2':40},
          }

trig_eff_measurements={
    'hpt2017':
          {'title':'High $p_T$ 2017',
           'trig':'HLT_Photon200',
           'ctrl':'HLT_Photon150',
           'ptmin':150,
           'onlyEB':False,
           'requireDijet':False,
           'vars':{'pt'         :{'bins':25,'mask_cuts':mask_cuts['hpt_pt'],'logx':True},
                   'eta'        :{'bins':25,'mask_cuts':mask_cuts['hpt_base'],'logx':False},
                   'PV_npvsGood':{'bins':10,'mask_cuts':mask_cuts['hpt_base'],'logx':False}
                  }
          },
    'lpt2017':
          {'title':'Low $p_T$ VBF 2017',
           'trig':'HLT_Photon75_R9Id90_HE10_IsoM_EBOnly_PFJetsMJJ300DEta3',
           'ctrl':'HLT_Photon50_R9Id90_HE10_IsoM',
           'ptmin':50,
           'onlyEB':True,
           'requireDijet':True,
           'vars':{'pt'         :{'bins':10,     'mask_cuts':mask_cuts['lpt_pt'],'logx':True},
                  'pt:mjj'      :{'bins':(6,6),'mask_cuts':mask_cuts['lpt_pt_mjj'],  'scale':'log:log'},
                  'mjj:ptj2'    :{'bins':(6,6),'mask_cuts':mask_cuts['lpt_mjj_ptj2'],'scale':'log:log'},
                   'ptj1'       :{'bins':10,'mask_cuts':mask_cuts['lpt_ptj1'],'logx':False},
                   'ptj2'       :{'bins':10,'mask_cuts':mask_cuts['lpt_ptj2'],'logx':True},
                   'mjj'        :{'bins':10,'mask_cuts':mask_cuts['lpt_mjj'],'logx':True},
                   'PV_npvsGood':{'bins':10,'mask_cuts':mask_cuts['lpt_base'],'logx':False},
                   'absdetajj'  :{'bins':10,'mask_cuts':mask_cuts['lpt_base'],'logx':False},
                  }
          },
     'lptnovbf2017':
          {'title':'$p_T>90$ 2017',
           'trig':'HLT_Photon90_R9Id90_HE10_IsoM',
           'ctrl':'HLT_Photon75_R9Id90_HE10_IsoM',
           'ptmin':75,
           'onlyEB':True,
           'requireDijet':False,
           'vars':{'pt'         :{'bins':10,'mask_cuts':mask_cuts['lpt_ptnovbf'],  'logx':True},
                   'PV_npvsGood':{'bins':10,'mask_cuts':mask_cuts['lpt_basenovbf'],'logx':False},
                   'absdetajj'  :{'bins':10,'mask_cuts':mask_cuts['lpt_basenovbf'],'logx':False},
                  }
          },
}    

trig_eff_measurements['hpt2016']=copy.deepcopy(trig_eff_measurements['hpt2017']) 
trig_eff_measurements['hpt2016']['title']='High $p_T$ 2016'
trig_eff_measurements['hpt2016']['trig']='HLT_Photon175'
trig_eff_measurements['hpt2016']['ctrl']='HLT_Photon90'
trig_eff_measurements['lpt2016']=copy.deepcopy(trig_eff_measurements['lpt2017']) 
trig_eff_measurements['lpt2016']['title']='Low $p_T$ VBF 2016'
trig_eff_measurements['lpt2016']['trig']='HLT_Photon75_R9Id90_HE10_Iso40_EBOnly_VBF'
trig_eff_measurements['lpt2016']['ctrl']='HLT_Photon50_R9Id90_HE10_IsoM' #'HLT_Photon50'

trig_eff_measurements['hpt2018']=copy.deepcopy(trig_eff_measurements['hpt2017']) 
trig_eff_measurements['hpt2018']['title']='High $p_T$ 2018'
trig_eff_measurements['hpt2018']['trig']='HLT_Photon200'
trig_eff_measurements['hpt2018']['ctrl']='HLT_Photon90'
trig_eff_measurements['lpt2018']=copy.deepcopy(trig_eff_measurements['lpt2017']) 
trig_eff_measurements['lpt2018']['title']='Low $p_T$ VBF 2018'
trig_eff_measurements['lpt2018']['trig']='HLT_Photon75_R9Id90_HE10_IsoM_EBOnly_PFJetsMJJ300DEta3'
trig_eff_measurements['lpt2018']['ctrl']='HLT_Photon50_R9Id90_HE10_IsoM' #'HLT_Photon50'

#collection of data, MC to be used for trigger measurements
from snippets.sample_definitions import _samples_Run2, getSamplesMatchingTags
data_for_trig_eff={
    '2016':{'data':{'BCDEFGH':{'title':'Data',
                                'chunks':['{{}}/{}.root'.format(x) for x in getSamplesMatchingTags(['data',2016])]},
                   },
            'mc':{'ewgjjpy8':{'title':'EWK $\gamma$jj Pythia',
                              'chunks':['{{}}/{}.root'.format(x) for x in getSamplesMatchingTags(['ewgjjpy8',2016])]},
                  'ewgjjhw': {'title':'EWK $\gamma$jj Herwig',
                              'chunks':['{{}}/{}.root'.format(x) for x in getSamplesMatchingTags(['ewgjjhw',2016])]},
                 }
           },    
    '2017':{'data':{'BCDEF':{'title':'Data',
                                'chunks':['{{}}/{}.root'.format(x) for x in getSamplesMatchingTags(['data',2017])]}
                   },
            'mc':{'ewgjjpy8':{'title':'EWK $\gamma$jj Pythia',
                              'chunks':['{{}}/{}.root'.format(x) for x in getSamplesMatchingTags(['ewgjjpy8',2017])]},
                  'ewgjjhw': {'title':'EWK $\gamma$jj Herwig',
                              'chunks':['{{}}/{}.root'.format(x) for x in getSamplesMatchingTags(['ewgjjhw',2017])]},
                 }
           },
    '2018':{'data':{'ABCD':{'title':'Data',
                            'chunks':['{{}}/{}.root'.format(x) for x in getSamplesMatchingTags(['data',2018])]}
                   },
            'mc':{'ewgjjpy8':{'title':'EWK $\gamma$jj Pythia',
                              'chunks':['{{}}/{}.root'.format(x) for x in getSamplesMatchingTags(['ewgjjpy8',2018])]},
                  'ewgjjhw': {'title':'EWK $\gamma$jj Herwig',
                              'chunks':['{{}}/{}.root'.format(x) for x in getSamplesMatchingTags(['ewgjjhw',2018])]},
                 }
           }
}
data_for_trig_eff['2017F']=copy.deepcopy(data_for_trig_eff['2017']) 
data_for_trig_eff['2017F']['data']['F']=data_for_trig_eff['2017F']['data'].pop('BCDEF')
data_for_trig_eff['2017F']['data']['F']['chunks']=[x for x in data_for_trig_eff['2017']['data']['BCDEF']['chunks'] if '2017_F' in x]

data_for_trig_eff['2017B']=copy.deepcopy(data_for_trig_eff['2017']) 
data_for_trig_eff['2017B']['data']['B']=data_for_trig_eff['2017B']['data'].pop('BCDEF')
data_for_trig_eff['2017B']['data']['B']['chunks']=[x for x in data_for_trig_eff['2017']['data']['BCDEF']['chunks'] if '2017_B' in x]



def getTrigMeasurementDefinitions():

    """ just a printout for the definitions above """
    
    import json 
    print(json.dumps(trig_eff_measurements, indent = 4))
    print(json.dumps(data_for_trig_eff,     indent = 4))


def getEfficiency(df,v,mask_den,mask_num,bins,method='beta',alpha=0.683):

    """
    wraps the procecedure to compute efficiency
    returns x,dx,eff,deff
    df: dataframe
    v: variable to count events
    mask_{den,num} : masks to apply for the denominator and numerator
    bins: the binning to use for the efficiency measurement
    method: beta=clopper-pearson 68%CL normal=asymptotic normal approximation
    alpha: CL
    """
    
    pts=[]
    
    #project denominator and denominator (1D or 2D cases)
    if isinstance(bins,list):
        
        x  = 0.5*(bins[0][1:]+bins[0][0:-1])
        dx = 0.5*(bins[0][1:]-bins[0][0:-1])
        dx = np.array([dx,dx])

        y  = 0.5*(bins[1][1:]+bins[1][0:-1])
        dy = 0.5*(bins[1][1:]-bins[1][0:-1])
        dy = np.array([dy,dy])
        
        pts=[x,dx,y,dy]

        vx,vy = v
        den,_,_ = np.histogram2d(df[mask_den][vx],df[mask_den][vy], bins=(x, y))
        num,_,_ = np.histogram2d(df[mask_num][vx],df[mask_num][vy], bins=(x, y))
        
    else:

        x  = 0.5*(bins[1:]+bins[0:-1])
        dx = 0.5*(bins[1:]-bins[0:-1])
        dx = np.array([dx,dx])
        
        pts=[x,dx]
        
        den,_ = np.array(np.histogram(df[mask_den][v],bins=bins))
        num,_ = np.array(np.histogram(df[mask_num][v],bins=bins))
        
    #compute efficiency
    num=np.array(num)
    den=np.array(den)
    eff = np.divide(num, den, out=np.zeros_like(num, dtype=float), where=den!=0)
        
    ci=proportion_confint(num, den, alpha=alpha, method=method)
    deff=np.array([eff-ci[0],ci[1]-eff])

    return (pts,eff,deff)


def getScaleFactor(eff_a,deff_a,eff_b,deff_b):

    """
    computes the scale factor from the ratio, simple error propagation applied
    eff_a/deff_a = numerator and associated error
    eff_b/deff_b = denominator and associated error
    """

    sf=np.divide(eff_a,eff_b,out=np.zeros_like(eff_b,dtype=float), where=eff_b!=0)
    
    max_deff_a=deff_a.copy()
    if deff_a.shape[0]==2:
        max_deff_a=np.maximum(deff_a[0,:],deff_a[1,:])
    rel_deff_a = np.divide(max_deff_a, eff_a, out=np.zeros_like(eff_a, dtype=float), where=eff_a!=0)
    dsf = np.power(rel_deff_a,2)

    max_deff_a=deff_a.copy()
    if deff_b.shape[0]==2:
        max_deff_b=np.maximum(deff_b[0,:],deff_b[1,:])
    rel_deff_b = np.divide(max_deff_b, eff_b, out=np.zeros_like(eff_b, dtype=float), where=eff_b!=0)
    dsf += np.power(rel_deff_b,2)
    
    dsf = sf*np.sqrt(dsf)
    return sf,dsf