import numpy as np
import copy
from scipy.stats import gamma 
import matplotlib.pyplot as plt
import mplhep as hep
plt.style.use([hep.style.ROOT, hep.style.firamath])

def getHessianShiftsOnFitResults(popt,pcov) :
    """
    finds orthogonal variations of the fit parameters based on the diagonalization of the covariance matrix
    """

    if np.isnan(popt).any() or np.isnan(pcov).any() or np.isinf(popt).any() or np.isinf(pcov).any():
        print('Could not run Hessian shifts - NaN values present, check fit results')
        return [popt,popt]
    
    #eigen values and vectors of the covariance matrix
    eigen_val, eigen_vec = np.linalg.eig(pcov.T)
    
    #sort eigen values ascending                
    idx = eigen_val.argsort()
    eigen_val = eigen_val[idx]
    eigen_vec = eigen_vec[:,idx]
    T=eigen_vec

    #transform parameters to diagonal basis                    
    popt_diag = np.dot(T, popt)

    #vary independently up and down and move back to original basis           
    popt_vars=[]
    for i in range(len(eigen_val)):
        popt_vars.append( np.array(popt_diag) )
        popt_vars[-1][i]=popt_vars[-1][i]+np.sqrt(eigen_val[i])
        popt_vars[-1]=np.dot(T,popt_vars[-1])

    return popt_vars
    

def countInHistogram(x,w,bins):
    
    """
    returns the bins, the counts per bins and the associated errors
    in case of unweighted data, the errors
    """
    if w is None:
        n, bins = np.histogram(x, bins=bins)
        #cf. (Asymmetric Error Bars for Poisson Event 
        #Counts)[https://twiki.cern.ch/twiki/bin/viewauth/CMS/PoissonErrorBars]
        alpha = 1 - 0.6827
        L = np.where(n==0,0.,gamma.ppf(alpha/2,n))
        U = gamma.ppf(1-alpha/2,n+1)
        n_err=np.stack( [n-L,U-n], axis=1)
    else:
        n, bins = np.histogram(x, bins=bins, weights=w)
        n_err = np.sqrt(np.histogram(x, bins=bins, weights=w**2)[0])
    return bins,n,n_err

def countIn2DHistogram(x,xbins,y,ybins,w):
    
    """
    returns the bins, the counts per bins and the associated errors
    in case of unweighted data, the errors
    """
    
    if w is None:
        n, xedges, yedges = np.histogram2d(x, y, bins=(xbins, ybins))
        #cf. (Asymmetric Error Bars for Poisson Event 
        #Counts)[https://twiki.cern.ch/twiki/bin/viewauth/CMS/PoissonErrorBars]
        alpha = 1 - 0.6827
        L = np.where(n==0,0.,gamma.ppf(alpha/2,n))
        U = gamma.ppf(1-alpha/2,n+1)
        n_err=np.stack( [n-L,U-n], axis=1)
    else:
        n, xedges, yedges = np.histogram2d(x, y, bins=(xbins, ybins), weights=w)        
        n_err = np.sqrt( np.histogram2d(x, y, bins=(xbins, ybins), weights=w**2 )[0] ) 
    return (xedges,yedges), n, n_err

def getLocalSignificance(h_sig,h_sig_unc,h_bkg,h_bkg_unc):

    """computes S/sqrt(B) and it's associated uncertainty"""
    
    sig = np.where(h_bkg>0, h_sig/np.sqrt(h_bkg), 0.)
    
    sig_unc  = np.where(h_sig!=0, (h_sig_unc/h_sig)**2, 0.)
    sig_unc += np.where(h_bkg!=0, (h_bkg_unc/(2*h_bkg))**2, 0.)
    sig_unc = sig * np.sqrt(sig_unc)
    
    return sig,sig_unc


def showComparisonPlot(stack=None, stack_labels=None,
                       data=None, data_label=None,
                       overlay=None, overlay_labels=None,
                       overlay_bottom=None, overlay_bottom_labels=None, simpleOverlay=True,
                       logy=False, logx=False, grid=True, scaleToData=False,
                       xlabel='x', ylabel='y', rylabel='Obs. / Pred.', ptitle=None,
                       yran=None, ryran=None, handle=None, xran=None,
                       legendFontSize=16,
                       outname=None):
    
    """
    classical data/MC stacked plot with a ratio panel
    stack and overlay are lists of (bins,counts,errors)
    data is a tuple of (bins,counts,(2,N) errors)
    the remaining parameters are self-explanatory
    based on this [example](https://github.com/nsmith-/mpl-hep/blob/master/binder/gallery.ipynb)
    if outname is given the plot is stored in .png/.pdf formats
    """
    
    kwargs={'figsize': (10, 8), 'gridspec_kw': {'height_ratios': (3, 1)},}
    fig, (ax, rax) = plt.subplots(2, 1, sharex=True, **kwargs)
    fig.subplots_adjust(hspace=.07)  # this controls the margin between the two axes

    ebar_style={'marker':'o','elinewidth':1,'capsize':1,'color':'k','ls':'none'}
    fill_style={'step':'post','facecolor':'lightgray','alpha':0.5,'edgecolor':'black', 'linewidth':0, 'hatch':'///'}
    overlay_style={'ls':'-','drawstyle':'steps','lw':2}
    overlay_ebar_style={'marker':'o','elinewidth':1,'capsize':1,'ls':'none'}

    #show the stack of histograms
    if stack:
    
        bins=stack[0][0]
        bin_centers=0.5*(bins[0:-1]+bins[1:])

        #stack the histograms
        sumw_stack = np.vstack([h[1] for h in  stack])
        sumw_stack = np.hstack([sumw_stack, sumw_stack[:,-1:]])  
        sumw_total = sumw_stack.sum(axis=0)
        if scaleToData:
            print('Not yet implemented')
            pass
        ax.stackplot(bins, sumw_stack, labels=stack_labels, step='post', edgecolor=(0, 0, 0, 0.5),)

        # Overlay an uncertainty hatch        
        sumw_unc = np.vstack([h[2] for h in  stack])
        sumw_unc = np.sqrt(np.sum(np.square(sumw_unc), axis=0))
        sumw_unc = np.hstack([sumw_unc, sumw_unc[-1]])
        ax.fill_between(x=bins, 
                        y1=sumw_total-sumw_unc, 
                        y2=sumw_total+sumw_unc,
                        label='Unc.', 
                        **fill_style
                       )
    
    #overlay
    if overlay:        
        for i in range(len(overlay)):   
            b,h,hunc=overlay[i]
            label=overlay_labels[i]
            ax.plot(b, np.insert(h, 0, h[0]), label=label, **overlay_style)

    # Draw the observation
    if data:
        bins=data[0]
        bin_centers=0.5*(bins[0:-1]+bins[1:])
        yerr=data[2]
        if yerr.shape[0]>2 : yerr=yerr.T
        ax.errorbar(x=bin_centers, 
                    y=data[1], 
                    yerr=yerr,
                    label=data_label, 
                    **ebar_style)
 
    #tweak the final formatting
    if yran:
        ax.set_ylim(*yran)
    else:
        ax.set_ylim(0, None)
    if xran:
        ax.set_xlim(*xran)
    ax.set_ylabel(ylabel)
    if logy: ax.set_yscale('log')
    if logx: ax.set_xscale('log')
    if grid: ax.grid()
    ax.legend(fontsize=legendFontSize)
    if handle: handle(ax)

    # Draw a comparison panel
    if stack: 
        # separate rather than combining (as per tradition)
        rax.fill_between(x=bins, 
                         y1=1 - sumw_unc/sumw_total, 
                         y2=1 + sumw_unc/sumw_total, 
                         **fill_style)
        
        #draw ratio
        if data:
            sumw_total = sumw_total[:-1]  # Take away that copy of the last bin
            rax.errorbar(x=bin_centers, y=data[1]/sumw_total, yerr=data[2].T/sumw_total, **ebar_style)

    #if also passed draw these
    if overlay_bottom:
        for i in range(len(overlay_bottom)):   
            b,h,hunc=overlay_bottom[i]
            label=overlay_bottom_labels[i] if overlay_bottom_labels else None
            if simpleOverlay:
                rax.plot(b, np.insert(h, 0, h[0]), label=label, **overlay_style)
            else:
                bin_centers=0.5*(b[0:-1]+b[1:])
                rax.errorbar(x=bin_centers, y=h, yerr=hunc, label=label, **overlay_ebar_style)
        if overlay_bottom_labels: rax.legend()
            
    if ryran:
        rax.set_ylim(*ryran)
    else:
        rax.set_ylim(0.45, 1.55)
    if xran:
        rax.set_xlim(*xran)
    rax.set_ylabel(rylabel)
    if logx: rax.set_xscale('log')
    if grid: rax.grid()
    rax.set_xlabel(xlabel)
    rax.autoscale(axis='x', tight=True) 
    
    if ptitle:
        hep.cms.label(loc=0,data=True,ax=ax,rlabel=ptitle)
    else:
        hep.cms.label(loc=0,data=True,ax=ax)
    fig.tight_layout()
            
    if outname:
        for ext in ['png','pdf']:
            plt.savefig('{}.{}'.format(outname,ext))
        plt.close()
        print('Plot saved in {}.{{png,pdf}}'.format(outname))

        
def showHistogram(h_list, labels_list,
                  xlabel=None, ylabel=None, ptitle=None,
                  logy=False, logx=False, yran=None,
                  outname=None):
    
    """simple display of several histograms overlaid"""

    x=h_list[0][0]
    counts=[h[1] for h in h_list]

    f, ax = plt.subplots()

    hep.histplot(counts,x, edges=True, stack=False, ax=ax,label=labels_list)
                
    hep.cms.label(loc=0,data=False,ax=ax,rlabel=ptitle)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)    
    if logy: ax.set_yscale('log')
    if logx: ax.set_xscale('log')
    if yran: ax.set_ylim(*yran)
    ax.legend()
    ax.grid()
    plt.tight_layout()
    plt.show()
    
    if outname:
        for ext in ['png','pdf']:
            plt.savefig('{}.{}'.format(outname,ext))
        plt.close()
        print('Plot saved in {}.{{png,pdf}}'.format(outname))
        
        
def show2DHistogram(histo,
                    xlabel=None, ylabel=None, ptitle=None, extra_txt=[],
                    outname=None,cmap='GnBu'):
    
    """shows a 2D histogram"""
        
    fig, ax = plt.subplots()    
    hep.hist2dplot(histo[1], histo[0][0],histo[0][1], labels=None, cmap=cmap)
    ax.plot([-1,2],[2,-1])
    
    for i,t in enumerate(extra_txt):
        ax.text(0.05,0.95-0.05*i, t, 
                horizontalalignment='left',
                verticalalignment='top', 
                transform=ax.transAxes)
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.grid()
    if ptitle:
        hep.cms.label(loc=0,data=True,ax=ax,rlabel=ptitle)
    else:
         hep.cms.label(loc=0,data=True,ax=ax)
    plt.tight_layout()
    plt.show()
    
    if outname:
        for ext in ['png','pdf']:
            plt.savefig('{}.{}'.format(outname,ext))
        plt.close()
        print('Plot saved in {}.{{png,pdf}}'.format(outname))