import ROOT
import pandas as pd
import numpy as np
import os

def prepareEWVJJSampleForAnalysis(samples,
                                  wgtSums,
                                  trigSFs,
                                  tags,
                                  indir,
                                  outpath,
                                  presel=['pt>75'],
                                  force=True):
    
    """applies standard selections and stores the final information for the analysis
    samples - the dict of samples
    wgtSums - the weight sums
    trigSFs - the object which computes the trigger scale factors
    tags    - the tags for the samples to process
    indir   - input directory with the RDataFrame snapshots
    outpath - output directory for the DataFrames
    presel  - vector of preselection cuts
    force - force the recreation
    """

    #luminosities per era for the high-pT and low-pT triggers
    hpt_lumi = {2016:35879, 2017:41528, 2018:59828}
    lpt_lumi = {2016:28412, 2017:7728,  2018:59828}

    trigs = { 2016:{'lpt':'HLT_Photon75_R9Id90_HE10_Iso40_EBOnly_VBF',
                    'hpt':'HLT_Photon175'},
              2017:{'lpt':'HLT_Photon75_R9Id90_HE10_IsoM_EBOnly_PFJetsMJJ300DEta3',
                    'hpt':'HLT_Photon200'},
              2018:{'lpt':'HLT_Photon75_R9Id90_HE10_IsoM_EBOnly_PFJetsMJJ300DEta3',
                    'hpt':'HLT_Photon200'} }
    
    isData=True if 'data' in tags else False

    ROOT.ROOT.EnableImplicitMT()
    print('Selecting samples for',tags,'with presel=',presel)
    chunks=[]
    for s,s_def in samples.items():

        tagsIn=all([t in s_def['tags'] for t in tags])
        if not tagsIn: continue
            
        #save chunk in parquet if not yet available
        out_chunk='{}.{}'.format(outpath,len(chunks))
        chunks.append(out_chunk)
        if not force and os.path.isfile(out_chunk):
            print('Chunk #{} already available'.format(len(chunks)))
            continue
        
        #check the year and build the normalization weight sum
        year=[x for x in s_def['tags'] if isinstance(x,int)][0]
        xsec=s_def['xsec'] 
        if isData:
            wgtSum=1.
        else:
            wgtSum=sum( [wgtSums[f][0] for f in s_def['dirs']] )

        #preselect
        print('\tyear={} xsec={} wgtSum={}'.format(year,xsec,wgtSum))        
        files=ROOT.vector('string')()            
        for f in s_def['dirs']:
            files.push_back( os.path.join(indir,f+'.root') )
            
        rdf=ROOT.RDataFrame('data',files)
        for p in presel:
            rdf=rdf.Filter(p)

        #convert to pandas
        print('\tconverting to pandas and adding extra info')
        columns=[str(c) for c in rdf.GetColumnNames()]
        df=pd.DataFrame(rdf.AsNumpy(),columns=columns)
        
        #useless columns
        cols_toDrop=['run','event']
        
        #simplify the trigger to result only in Pass_{lpt,hpt}
        trigCols=[c for c in df.columns if c.find('Pass_')==0]
        for t,tname in trigs[year].items():
            tname='Pass_{}'.format(tname)
            df['Pass_{}'.format(t)] = 0 if not tname in trigCols else df[tname]
        cols_toDrop+=trigCols

        if isData:
            df['wgt']=1.
        else:
           
            #compute trigger weights
            kwargs={'year':year,'onlySF':False}
            lpt_trigSF = pd.DataFrame( df.apply( lambda x : trigSFs.getScaleFactorAndUncertainties(row=x,lpt=True, **kwargs), axis=1 ))
            lpt_trigSF = pd.DataFrame(lpt_trigSF[0].to_list(), columns=['cen','Up','Dn'] )
            hpt_trigSF = pd.DataFrame( df.apply( lambda x : trigSFs.getScaleFactorAndUncertainties(row=x,lpt=False,**kwargs), axis=1 ))
            
            hpt_trigSF = pd.DataFrame(hpt_trigSF[0].to_list(), columns=['cen','Up','Dn'] )

            #build final event weight
            df['genWeight'] = (xsec/wgtSum)*df['genWeight']
            common_wgt = df['genWeight']*df['puWeight']*df['vjj_photon_effWgt']
            df['hpt_wgt'] = hpt_trigSF['cen']*hpt_lumi[year]*common_wgt
            df['lpt_wgt'] = lpt_trigSF['cen']*lpt_lumi[year]*common_wgt
            cols_toDrop += ['puWeight','vjj_photon_effWgt']            
            if year!=2018 : 
                df['hpt_wgt'] = df['hpt_wgt']*df['PrefireWeight'] 
                df['lpt_wgt'] = df['lpt_wgt']*df['PrefireWeight'] 
                cols_toDrop.append('PrefireWeight')

            #ad-hoc flag to emulate prescaling in the MC
            df['lpt_available'] = np.random.uniform(0,1)
            df['lpt_available'] = np.where(df['lpt_available']<lpt_lumi[year]/hpt_lumi[year],1,0)

            #weight scale-factors for systematics
            variedWeights={'puWeight':['Up','Down'],
                           'vjj_photon_effWgt':['Up','Dn'],}
            if year!=2018: variedWeights['PrefireWeight']=['_Up','_Down']
            for w,wv in variedWeights.items():
                for iv in wv:
                    finalVarName='wgtSF_{}_{}'.format(w,iv)
                    finalVarName=finalVarName.replace('__','_')
                    finalVarName=finalVarName.replace('Down','Dn')
                    df[finalVarName]=df['{}{}'.format(w,iv)]/df[w]
            for wv in ['Up','Dn']:
                df['wgtSF_lpt_trigWgt{}'.format(wv)]=lpt_trigSF[wv]/lpt_trigSF['cen']
                df['wgtSF_hpt_trigWgt{}'.format(wv)]=hpt_trigSF[wv]/hpt_trigSF['cen']
            del lpt_trigSF
            del hpt_trigSF
            
        #drop columns which are not needed
        df.drop(columns=cols_toDrop,inplace=True)

        print('\tsaving chunk in hdf format')
        df.to_hdf(chunks[-1],key='data')
        
    ROOT.ROOT.DisableImplicitMT()
                     
    #now merge all of them
    print('\tmerging {} chunks and saving in hdf format'.format(len(chunks)))
    df = pd.concat([pd.read_hdf(fp) for fp in chunks])
    df.to_hdf(outpath,key='data')
    for fp in chunks: os.system('rm {}'.format(fp))
        
    #remove the dataframe
    del df