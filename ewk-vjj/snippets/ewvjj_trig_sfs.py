import pickle
import numpy as np

class EWVJJTriggerScaleFactor:
    
    """
    Wraps the handling of trigger scale factors
    """
    
    def __init__(self,url):
        
        """parses the pickle file with the scale factor parameterizations"""
        
        with open(url,'rb') as pck:
            self.params=pickle.load(pck)
    
    def getTriggerScaleFactor(self,x,cat,var):
    
        """
        gets the appropriate trigger scale factor as function of the reconstructed kinematics
        x - the reconstucted kinematics value
        cat - the category
        var - the variable to use
        e.g. getTriggerScaleFactor(mjj,'lpt2016','mjj')    
        returns the central scelar factor, the stat uncertainty and the systematic uncertainty
        """
    
        def sfparam(x,k,mu,sigma):
            z=(x-mu)/sigma
            return k/(1+np.exp(-z))
    
        #central value
        key=(var,cat,'ewgjjhw')
        popt,pcov=self.params[key]
        ycen=sfparam(x, *popt)

        #stat uncertainty
        popt_up = [popt[i]+np.sqrt(pcov[i,i]) for i in range(len(popt))]
        yup     = sfparam(x, *popt_up)
        popt_dn =[popt[i]-np.sqrt(pcov[i,i]) for i in range(len(popt))]
        ydn     = sfparam(x, *popt_dn)

        #py8-hw difference
        key=(var,cat,'ewgjjpy8')
        popt,pcov=self.params[key]
        yalt=sfparam(x, *popt)
    
        return ycen,max(abs(yup-ycen),abs(ycen-ydn)),abs(ycen-yalt)
        
    
    def getScaleFactorAndUncertainties(self,lpt,year,**kwargs):
        
        """
        computes the trigger scale factor and its uncertainty, 
        arguments can be the kinematics or a DataFrame row
        """
        
        try:
            pt=kwargs['row']['pt']
            mjj=kwargs['row']['mjj']
            ptj2=kwargs['row']['ptj2']
        except:
            pt=kwargs['pt']
            mjj=kwargs['mjj']
            ptj2=kwargs['ptj2']
        
        sf,unc_stat,unc_syst=1.0,0.0,0.0
        if lpt and pt>50 and mjj>200:
            sf,unc_stat,unc_syst = self.getTriggerScaleFactor(mjj,'lpt{}'.format(year),'mjj')

            #add in quadrature the difference when parameterized as function of pt(j2)
            sf_ptj2,_,_ = self.getTriggerScaleFactor(mjj,'lpt{}'.format(year),'ptj2')
            unc_syst=np.sqrt(unc_syst**2+(sf-sf_ptj2)**2)

        if not lpt and pt>150:
            sf,unc_stat,unc_syst = self.getTriggerScaleFactor(pt,'hpt{}'.format(year),'pt')
        
        #total uncertainty
        tot_unc=np.sqrt(unc_stat**2+unc_syst**2)
        
        try:
            if kwargs['onlySF']: return sf
        except:
            pass
        
        return sf,sf+tot_unc,max(sf-tot_unc,0.0)
    
def main():
    
    sf=EWVJJTriggerScaleFactor('/eos/user/p/psilva/www/SMP-19-005/trig/2021Feb05//triggerSFparam.pck')
    for year in range(2016,2019):
        print('Testing year={}'.format(year))
        result=sf.getScaleFactorAndUncertainties(pt=200,mjj=600,ptj2=30,year=year,lpt=True)
        print('\tSF(low pt)=',result)
        result=sf.getScaleFactorAndUncertainties(pt=200,mjj=0,ptj2=0,year=year,lpt=False)
        print('\tSF(high pt)=',result)

if __name__ == "__main__":
    main()