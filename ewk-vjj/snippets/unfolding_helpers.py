import numpy as np
from scipy.interpolate import interp1d
import root_numpy
import ROOT

def defineFairBinning(gen,rec,minimize_cond=True,max_cond=50,handle=None):
    
    """
    given an array of generated and reconsutructed quantities determine a fair binning for unfolding
        * loop over 1% quantiles
        * check if one can sub-divide n=floor(bin width / resol)
            * if n=0 bin width is smaller than resolution, move to next quantile
            * if n>0 generate additional interquantiles in this range
        * after the bins are merged in order to minimize the condition number of the migration matrix
        * an handle (e.g. for plotting) can be passed to use further the intermediate quantities which won't be returned
    """
    
    #find 1% quantiles
    q=np.percentile(gen,np.linspace(0,100,100))
    
    #scan 10 points wih equal statistics to find resolution
    gen_avg=[]
    res=[]
    for thr in q.reshape(10,10):
        thr_min,thr_max=thr[0],thr[-1]
        mask=(gen>=thr_min) & (gen<thr_max)
        gen_avg.append( np.mean(gen[mask]) )
        res.append( np.mean(np.abs(rec[mask]-gen[mask])) )
    
    #interpolate (add also extremities)
    gen_avg = [q[0]] + gen_avg + [q[-1]]
    res = [res[0]] + res + [res[-1]]
    res_spline = interp1d(gen_avg,res,kind='linear',fill_value='extrapolate')
         
    #determine fair binning
    bins=np.array( [q[0]] )
    for thr in q[1:-1]:
        
        bin_wid = (thr-bins[-1])
        bin_cen = 0.5*(bins[-1]+thr)
        exp_res = res_spline(bin_cen)
        n = int(bin_wid/exp_res)
        if n==0: continue
        mask=(gen>=bins[-1]) & (gen<thr)
        iq=np.percentile(gen[mask],np.linspace(0,100,n))
        bins=np.concatenate( (bins, iq[1:]), axis=0 )
    
    bins=np.append(bins,q[-1])
    
    #reconstruction-level bins (wid = 1/2 of gen)
    bwid = 0.5*(bins[1:]-bins[0:-1])
    rec_bins = (bins[0:-1]+bwid)
    rec_bins = np.concatenate( (bins,rec_bins), axis=0)
    rec_bins = rec_bins[ np.argsort(rec_bins) ]
    
    #rebin trying to obtain a better condition number for the migration matrix stops if the condition number starts 
    #to increase or a satisfatory value is attained
    if minimize_cond:
        max_gen=bins[-1]
        max_rec=rec_bins[-1]
    
        while True:
            
            #compute current condition number
            mig = np.histogram2d(gen,rec,bins=(bins,rec_bins))[0]
            condK = np.linalg.cond(mig)
            if condK<max_cond: break
            
            #rebin by a factor of 2
            try_bins=bins[::2]
            if try_bins[-1] != max_gen : try_bins = np.append(try_bins,max_gen)
            try_rec_bins=rec_bins[::2]
            if try_rec_bins[-1] != max_rec : try_rec_bins = np.append(try_rec_bins,max_rec)
    
            try_mig = np.histogram2d(gen,rec,bins=(try_bins,try_rec_bins))[0]
            try_condK = np.linalg.cond(try_mig)
            if try_condK<condK:
                bins=try_bins.copy()
                rec_bins=try_rec_bins.copy()
    
    
    #further post-processing with handle
    if handle:
        kwargs={'q':q,'gen_avg':gen_avg,'res_spline':res_spline,'bins':bins}
        handle(kwargs)
    
    return {'gen':bins,'rec':rec_bins}


class TUnfoldWrapper:
    
    """"
    This class wraps the operations used to unfold with TUnfoldDensity
    The options used are expeced to yield good results if the migration matrix is well conditioned
    """
    
    def __init__(self,
                 mig,
                 tau=-1,
                 regMode=ROOT.TUnfold.kRegModeCurvature,
                 constraint=ROOT.TUnfold.kEConstraintNone,
                 densityFlags=ROOT.TUnfoldDensity.kDensityModeNone):
    
        self.tau = tau
        nx,ny=mig.shape
        hmig=ROOT.TH2F('mig','mig',nx,0,nx,ny,0,ny)
        root_numpy.array2hist(mig,hmig)
        self.unfold = ROOT.TUnfoldDensity(hmig,
                                          ROOT.TUnfold.kHistMapOutputHoriz,
                                          regMode,
                                          constraint,
                                          densityFlags)
        
        self.cov=None
        self.folded = None
        self.unfolded = None
        self.unfolded_unc = None
        self.chi2a = None
        self.chi2l = None
        self.ndf = None
        
        
   
    def scanForBestTau(self,nScan=100,tauMin=1e-4,tauMax=0.1,tauFlag=ROOT.TUnfoldDensity.kEScanTauRhoAvg):
        
        """calls the ScanTau method from TUnfoldDensity to find the value which yields the lowest average correlation"""

        rhoScanSpline = ROOT.TSpline()
        iBest = self.unfold.ScanTau(nScan,tauMin,tauMax,rhoScanSpline,tauflag)  
        self.tau=float(unfold.GetTau())

        #get the avg rho for the best tau
        #x=ROOT.Double(0)
        #y=ROOT.Double(0)                  
        #rhoScanSpline.GetKnot(iBest,x,y)
        #setattr('avgrho',self,float(y))
                  
        #save the spline scan
        #rhoScan=[]
        #for i in range(nScan):
        #    rhoScanSpline.GetKnot(i,x,y)
        #    rhoScan.append([float(x),float(y)])
        #setattr('rhoScan',self,np.array(rhoScan))

        return self.tau
        
    def runUnfold(self,data,scaleBias=1) :

        """
        performs the unfolding on a reconstructed data distribution
        """

        nx=data.shape[0]
        datah=ROOT.TH1F('data','data',nx,0,nx)
        root_numpy.array2hist(data,datah)
        status = self.unfold.SetInput(datah,scaleBias)
        if status!=0:
            raise ValueError('TUnfoldDensity failed with status {}'.format(status))

        if self.tau<0:
            self.scanForBestTau()
              
        self.unfold.DoUnfold(self.tau)
        
        self.cov          = root_numpy.hist2array( self.unfold.GetEmatrixTotal("EmatTotal") )
        self.folded       = root_numpy.hist2array( self.unfold.GetFoldedOutput('folded_data') )#,'','','',False) )
        self.unfolded     = root_numpy.hist2array( self.unfold.GetOutput('unfolded_data') ) #"",0,0,0,"",False) )
        self.unfolded_unc = np.sqrt(self.cov.diagonal())
                  
        datah.Delete()
        
        return self.unfolded,self.unfolded_unc