import ROOT
import numpy as np
import os
import re

try:
    from roofit_helpers import MyRooFitter
except:
    from snippets.roofit_helpers import MyRooFitter

    
class PUJetsFitter(MyRooFitter):
    
    """This class constructs the workspace with the physics model used to estimate the contribution from pileup jets"""
    
    def __init__(self,outdir,verbose=False):
        
        super().__init__(outdir,verbose)
      
    def runFit(self,hists_dict,fitName,ncpu=2,others_constraint=0.2,smear=False):
        
        """
        starts the workspace
        hists_dict is a list of dicts with histograms (numpy arrays) for each slice 
        which is expected be filled as follows
        [{ 'avg':float, 'prompt_loose':histo, 'prompt_tight':histo, 'data_loose':histo, 'data_tight':histo}, ... ]
        fitName it's just a name for the final workspace file
        
        
        data =  N * [ (1-alpha*b0)*nopu+alpha*b0*pu] + Nothers * others 
        sf=a*detajj+b
        
        """
       
        data=hists_dict['data']
        n_categories,nbins=data.shape
        print('Staring workspace for {} with nbins={} and {} categories'.format(fitName,nbins,n_categories))
        
        #start a workspace
        ws=ROOT.RooWorkspace("w")
            
        #define the observable
        ws.factory('x[0,0,{}]'.format(nbins))       
        obsSet=ROOT.RooArgSet(ws.var('x'))
        
        #slicing observable and category names
        ws.factory('y[0,10]')
        category_names=['cat{}'.format(i) for i in range(n_categories)]

        ws.factory("RooFormulaVar::alpha('(@0+@1*@2)',{a[1,0,10],b[0,-10,10],y})")
        #ws.factory("RooFormulaVar::alpha('(@0+@1*@3+@2*@3*@3)',{a[1,0,10],b[0,-10,10],c[0,-10,10],y})")
        
        #resolution
        if smear:
            ws.factory("Gaussian::resol(x,0.,resol_sigma[1,1e-3,2])") 
        
        #import histograms and define PDFs in the RooWorkspace 
        #(needs TH1F -> RooDataHist conversion first)
        #yields per category are also stored
        dataMap=ROOT.MappedRooDataHist()
        yields={}
        for icat in range(n_categories):
            
            yval=hists_dict['profile'][icat]
            for hname in ['data','prompt','pu','others']:
                
                htag='{}_cat{}'.format(hname,icat)
                
                #import histogram and save yields
                rh=self.fillROOTHisto(hists_dict[hname][icat,:],htag)
                getattr(ws,'import')(ROOT.RooDataHist(htag,htag,ROOT.RooArgList(obsSet),rh))
                
                yields[(hname,icat)]=rh.Integral()
            
                #normalize and import as PDF for prompt and control region in data
                if hname !='data':
                    rh.Scale(1./yields[(hname,icat)])
                    for xbin in range(1,rh.GetNbinsX()+1):
                        if rh.GetBinContent(xbin)>0 : continue
                        rh.SetBinContent( xbin,1e-6 )
                    getattr(ws,'import')(
                        ROOT.RooDataHist('norm_'+htag,
                                         'norm_'+htag,
                                         ROOT.RooArgList(obsSet),
                                         rh) 
                    )
                    if hname=='data' or not smear:
                        getattr(ws,'import')(
                            ROOT.RooHistPdf('pdf_{}'.format(htag),
                                            'pdf_{}'.format(htag),
                                            obsSet,
                                            ws.data('norm_'+htag))
                        )
                    else:
                        getattr(ws,'import')(
                            ROOT.RooHistPdf('pdf_raw_{}'.format(htag),
                                            'pdf_raw_{}'.format(htag),
                                            obsSet,
                                            ws.data('norm_'+htag))
                        )
                        ws.factory('FCONV::pdf_{0}(x,pdf_raw_{0},resol)'.format(htag))
                        
      
                #free memory
                rh.Delete()
            
                #in each slice the data will correspond to a fixed value of y and stored in a binned map
                if hname!='data':  continue                    
                ws.factory('y_cat{}[{}]'.format(icat,yval))
                ws.var('y_cat{}'.format(icat)).setConstant(True)
                binnedData=ws.data(htag)
                dataMap.add('cat{}'.format(icat),binnedData)
                
                #add also the pileup scaling model for this category
                ws.factory("EDIT::alpha_cat{0}(alpha,y=y_cat{0})".format(icat))

            #create final fit model for single category
            # N * [ (1-alpha*beta0)*nopu+alpha*beta0*pu] + Nothers * others 
            
            ndata   = yields[('data',icat)]

            #signal component 
            nprompt = yields[('prompt',icat)]
            npu     = yields[('pu',icat)]
            nsig    = npu+nprompt
            b0      = npu/nsig
            ws.factory("RooFormulaVar::beta_cat{0}("
                       "'TMath::Min( TMath::Max( @0*@1, 0.5*@1 ), 5*@1 )',"
                       "{{ alpha_cat{0},beta0_cat{0}[{1}] }} )".format(icat,b0))
            ws.factory("SUM::pdf_signal_cat{0}("
                       "beta_cat{0}*pdf_pu_cat{0},"
                       "pdf_prompt_cat{0})".format(icat) )
            ws.factory('N_cat{}[{},0,{}]'.format(icat,nsig,1.2*ndata))
            
            #others (will float constrainted by 20% unc. nuisance)
            nothers = yields[('others',icat)]
            ws.factory("Gaussian::constr_others_cat{0}(0.,theta_others_cat{0}[-5,5],1.)".format(icat))            
            ws.factory("RooFormulaVar::Nothers_cat{0}("
                       "'{1}*(1+@0*{2})',"
                       "{{ theta_others_cat{0} }} )".format(icat,nothers,others_constraint) )

            #category raw and (final) constrained models
            ws.factory("SUM::raw_model_cat{0}("
                       "N_cat{0}*pdf_signal_cat{0},"
                       "Nothers_cat{0}*pdf_others_cat{0})".format(icat) )            
            ws.factory("PROD::model_cat{0}( raw_model_cat{0}, constr_others_cat{0} )".format(icat) )

        #after creating the PDFs for the individual categories, join them in a simulatenous model
        ycats='ycats[{}]'.format( ','.join(category_names) )
        ypdfs=','.join( ['cat{0}=model_cat{0}'.format(i) for i in range(n_categories)] )
        ws.factory('SIMUL::model({},{})'.format(ycats,ypdfs))
         
        #build a combined data histogram for several categories
        getattr(ws,'import')( ROOT.RooDataHist("data","data",
                                                    ROOT.RooArgList(obsSet),
                                                    ws.cat('ycats'),
                                                    dataMap.get()) )
                   
        #debug
        if self.verbose:
            ws.Print('v')

        self.garbage.append(ws)
        
        #perform the fit
        pdf=ws.pdf('model')
        fit_params = pdf.getParameters(ws.var('x'));        
        data=ws.data('data')
        fitResult=pdf.fitTo(data,ROOT.RooFit.Extended(),ROOT.RooFit.Save(True),ROOT.RooFit.NumCPU(ncpu))
        ws.saveSnapshot("fitresult_params", fit_params, True)
        getattr(ws,'import')( fitResult )
        
        #save to file
        fit_file=os.path.join(self.outdir,'{}.root'.format(fitName))
        ws.writeToFile( fit_file )
        print('Workspace and fit results have been stored in',fit_file)
        
        return fit_file
    
    def getPOIs(self,url):
        
        """gets the PU fraction in each bin"""
        
         #get the workspace and load the fit results
        fIn=ROOT.TFile.Open(url)
        w=fIn.Get('w')
        w.loadSnapshot('fitresult_params')
        fit_result=w.genobj('fitresult_model_data')

        #categories to project
        ycats=w.cat('ycats')
        category_list=[s.first for s in ycats.states()]

        #loop over categories and get POIs
        y = []
        beta0,beta,beta_unc = [],[],[]
        for category in category_list:        
            y.append(w.var('y_{}'.format(category)).getVal())
            beta0.append( w.var('beta0_{}'.format(category)).getVal())
            beta_func = w.function('beta_{}'.format(category))
            beta.append( beta_func.getVal() )
            beta_unc.append( beta_func.getPropagatedError(fit_result) )
                
        fIn.Close()
        
        pois={'y':np.array(y),
              'beta_0':np.array(beta0),
              'beta':np.array(beta),
              'beta_unc':np.array(beta_unc)}
        pois['sf_beta']=pois['beta']/pois['beta_0']
        pois['sf_betaUnc']=pois['beta_unc']/pois['beta_0']
        return pois
        
    def showFitResult(self,url,title,outname):
        
        """shows the canvas with the final values of the fit used for the PDF"""

        super().showFitResult(url,title,outname,
                              fit_results_snapshot="fitresult_params",
                              data_name='data',
                              model_name='model',
                              model_title='=0 PU',
                              components_list=['pdf_pu_{0}','pdf_pu_{0},pdf_others_{0}'], 
                              components_titles=['#geq1 PU','others'],
                              xlabel='R_{1} (bin number)', 
                              ylabel='Events',
                              slicevar_title='#Delta#eta(jj)',
                              params_caption_handle=None)
                            
          
def main():
    
    import os
    import numpy as np
    import pandas as pd
    import pickle
    import pprint

    cat='lpt'
    templates_url='/eos/user/p/psilva/data/ewk-vjj/tight/Histograms/2018/pujets_templates.pck'
    with open(templates_url,'rb') as fin:
        templates=pickle.load(fin)[cat]
                            
    #call the template fitter to extract the pileup jet scaling component
    pujf=PUJetsFitter('/eos/user/p/psilva/data/ewk-vjj/test/',verbose=True)
    ws_url=pujf.runFit(hists_dict=templates,fitName='testpufit')
    pujf.showFitResult(ws_url,'test','testpufit')
    pprint.pprint(pujf.getPOIs(ws_url),width=1)


if __name__ == "__main__":
    main()
    