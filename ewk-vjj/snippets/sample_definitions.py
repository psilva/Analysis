#!/usr/bin/env python

"""
This file contains a long list of samples to be used in analysis. 
The samples are organized in the _samples dict where the key is the name of the sample in MCM.
The contents corresponding to each key are a dict with the following items:
* xsec : the cross section in pb
* tags : use this list to later collect only the samples you are interested in
* dirs : the directories where the samples are stored in EOS
A second dict called _samplesTitles stores a latex-based title for each tag which may be found in _samples
"""

#list of samples
_samples_Run2={
'GJets_HT-40To100_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016':{
    'xsec':20660,
    'tags':['gjetslo','gjets40to100lo',2016],
    'dirs':['crab_GJets_HT-40To100_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016_ext1_v1'],
    },
'GJets_HT-40To100_TuneCP5_13TeV-madgraphMLM-pythia8_2017':{
    'xsec':20660,
    'tags':['gjetslo','gjets40to100lo',2017],
    'dirs':['crab_GJets_HT-40To100_TuneCP5_13TeV-madgraphMLM-pythia8_2017_pmx_ext1_v1'],
    },
'GJets_HT-40To100_TuneCP5_13TeV-madgraphMLM-pythia8_2018':{
    'xsec':20660,
    'tags':['gjetslo','gjets40to100lo',2018],
    'dirs':['crab_GJets_HT-40To100_TuneCP5_13TeV-madgraphMLM-pythia8_2018_v1'],
    },
'GJets_HT-100To200_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016':{
    'xsec':9235,
    'tags':['gjetslo','gjets100to200lo',2016],
    'dirs':['crab_GJets_HT-100To200_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016_ext1_v1',
            'crab_GJets_HT-100To200_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016_v1'],
    },
'GJets_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8_2017':{
    'xsec':9235,
    'tags':['gjetslo','gjets100to200lo',2017],
    'dirs':['crab_GJets_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8_2017_pmx_v1'],
    },
'GJets_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8_2018':{
    'xsec':9235,
    'tags':['gjetslo','gjets100to200lo',2018],
    'dirs':['crab_GJets_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8_2018_v1'],
    },
'GJets_HT-200To400_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016':{
    'xsec':2303,
    'tags':['gjetslo','gjets200to400lo',2016],
    'dirs':['crab_GJets_HT-200To400_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016_ext1_v1',
            'crab_GJets_HT-200To400_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016_v1'],
    },
'GJets_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8_2017':{
    'xsec':2303,
    'tags':['gjetslo','gjets200to400lo',2017],
    'dirs':['crab_GJets_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8_2017_pmx_v1'],
    },
'GJets_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8_2018':{
    'xsec':2303,
    'tags':['gjetslo','gjets200to400lo',2018],
    'dirs':['crab_GJets_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8_2018_v1'],
    },
'GJets_HT-400To600_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016':{
    'xsec':274.4,
    'tags':['gjetslo','gjets400to600lo',2016],
    'dirs':['crab_GJets_HT-400To600_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016_ext1_v1',
            'crab_GJets_HT-400To600_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016_v1']
    },
'GJets_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8_2017':{
    'xsec':274.4,
    'tags':['gjetslo','gjets400to600lo',2017],
    'dirs':['crab_GJets_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8_2017_v1'],
    },
'GJets_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8_2018':{
    'xsec':274.4,
    'tags':['gjetslo','gjets400to600lo',2018],
    'dirs':['crab_GJets_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8_2018_v1'],
    },
'GJets_HT-600ToInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016':{
    'xsec':93.51,
    'tags':['gjetslo','gjets400to600lo',2016],
    'dirs':['crab_GJets_HT-600ToInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016_ext1_v1','crab_GJets_HT-600ToInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_2016_v1']
    },
'GJets_HT-600ToInf_TuneCP5_13TeV-madgraphMLM-pythia8_2017':{
    'xsec':93.51,
    'tags':['gjetslo','gjets400to600lo',2017],
    'dirs':['crab_GJets_HT-600ToInf_TuneCP5_13TeV-madgraphMLM-pythia8_2017_v1'],
    },
'GJets_HT-600ToInf_TuneCP5_13TeV-madgraphMLM-pythia8_2018':{
    'xsec':93.51,
    'tags':['gjetslo','gjets400to600lo',2018],
    'dirs':['crab_GJets_HT-600ToInf_TuneCP5_13TeV-madgraphMLM-pythia8_2018_ext1_v1'],
    },
'GJets_SM_5f_TuneEE5C_EWK_13TeV-madgraph-herwigpp_2016':{
    'xsec':29.65,
    'tags':['ewgjjhw',2016],
    'dirs':['crab_GJets_SM_5f_TuneEE5C_EWK_13TeV-madgraph-herwigpp_2016_v1'],
    },
'GJets_SM_5f_TuneEE5C_EWK_13TeV-madgraph-herwigpp_2017':{ 
    'xsec':29.65,
    'tags':['ewgjjhw',2017],
    'dirs':['crab_GJets_SM_5f_TuneEE5C_EWK_13TeV-madgraph-herwigpp_2017_v1'],
    },
'GJets_SM_5f_TuneCH3_EWK_13TeV-madgraph-herwig7_2018':{
    'xsec':29.65,
    'tags':['ewgjjhw',2018],
    'dirs':['crab_GJets_SM_5f_TuneCH3_EWK_13TeV-madgraph-herwig7_2018_v1'], 
    },
 'GJets_SM_5f_TuneCUETP8M1_EWK_13TeV-madgraph-pythia8_2016':{
    'xsec':29.65,
    'tags':['ewgjjpy8',2016],
    'dirs':['crab_GJets_SM_5f_TuneCUETP8M1_EWK_13TeV-madgraph-pythia8_2016_v1'],
    },
'GJets_SM_5f_TuneCP5_EWK_13TeV-madgraph-pythia8_2017':{ 
    'xsec':29.65,
    'tags':['ewgjjpy8',2017],
    'dirs':['crab_GJets_SM_5f_TuneCP5_EWK_13TeV-madgraph-pythia8_2017_ext1_v1'],
    },
'GJets_SM_5f_TuneCP5_EWK_13TeV-madgraph-pythia8_2018':{
    'xsec':29.65,
    'tags':['ewgjjpy8',2018],
    'dirs':['crab_GJets_SM_5f_TuneCP5_EWK_13TeV-madgraph-pythia8_2018_v1'], 
    },
'QCD_Pt-50to80_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016':{
    'xsec':1989000.,
    'tags':['qcd',2016],
    'dirs':['crab_QCD_Pt-50to80_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016_ext1_v1','crab_QCD_Pt-50to80_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016_v1'], 
    },
'QCD_Pt-50to80_EMEnriched_TuneCP5_13TeV_pythia8_2017':{
    'xsec':1989000.,
    'tags':['qcd',2017],
    'dirs':['crab_QCD_Pt-50to80_EMEnriched_TuneCP5_13TeV_pythia8_2017_v1'], 
    },
'QCD_Pt-50to80_EMEnriched_TuneCP5_13TeV_pythia8_2018':{
    'xsec':1989000.,
    'tags':['qcd',2018],
    'dirs':['crab_QCD_Pt-50to80_EMEnriched_TuneCP5_13TeV_pythia8_2018_v1'], 
    },
'QCD_Pt-80to120_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016':{
    'xsec':366500.,
    'tags':['qcd',2016],
    'dirs':['crab_QCD_Pt-80to120_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016_ext1_v1','crab_QCD_Pt-80to120_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016_v1'], 
    },
'QCD_Pt-80to120_EMEnriched_TuneCP5_13TeV_pythia8_2017':{
    'xsec':366500.,
    'tags':['qcd',2017],
    'dirs':['crab_QCD_Pt-80to120_EMEnriched_TuneCP5_13TeV_pythia8_2017_v1'], 
    },
'QCD_Pt-80to120_EMEnriched_TuneCP5_13TeV_pythia8_2018':{
    'xsec':366500.,
    'tags':['qcd',2018],
    'dirs':['crab_QCD_Pt-80to120_EMEnriched_TuneCP5_13TeV_pythia8_2018_v1'], 
    },
'QCD_Pt-120to170_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016':{
    'xsec':66490.,
    'tags':['qcd',2016],
    'dirs':['crab_QCD_Pt-120to170_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016_v1'], 
    },
'QCD_Pt-120to170_EMEnriched_TuneCP5_13TeV_pythia8_2017':{
    'xsec':66490.,
    'tags':['qcd',2017],
    'dirs':['crab_QCD_Pt-120to170_EMEnriched_TuneCP5_13TeV_pythia8_2017_v1'], 
    },
'QCD_Pt-120to170_EMEnriched_TuneCP5_13TeV_pythia8_2018':{
    'xsec':66490.,
    'tags':['qcd',2018],
    'dirs':['crab_QCD_Pt-120to170_EMEnriched_TuneCP5_13TeV_pythia8_2018_v1'], 
    },
'QCD_Pt-170to300_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016':{
    'xsec':16480.,
    'tags':['qcd',2016],
    'dirs':['crab_QCD_Pt-170to300_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016_v1'], 
    },
'QCD_Pt-170to300_EMEnriched_TuneCP5_13TeV_pythia8_2017':{
    'xsec':16480.,
    'tags':['qcd',2017],
    'dirs':['crab_QCD_Pt-170to300_EMEnriched_TuneCP5_13TeV_pythia8_2017_v1'], 
    },
'QCD_Pt-170to300_EMEnriched_TuneCP5_13TeV_pythia8_2018':{
    'xsec':16480.,
    'tags':['qcd',2018],
    'dirs':['crab_QCD_Pt-170to300_EMEnriched_TuneCP5_13TeV_pythia8_2018_v1'], 
    },
'QCD_Pt-300toInf_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016':{
    'xsec':1099.,
    'tags':['qcd',2016],
    'dirs':['crab_QCD_Pt-300toInf_EMEnriched_TuneCUETP8M1_13TeV_pythia8_2016_v1'], 
    },
'QCD_Pt-300toInf_EMEnriched_TuneCP5_13TeV_pythia8_2017':{
    'xsec':1099.,
    'tags':['qcd',2017],
    'dirs':['crab_QCD_Pt-300toInf_EMEnriched_TuneCP5_13TeV_pythia8_2017_v1'], 
    },
'QCD_Pt-300toInf_EMEnriched_TuneCP5_13TeV_pythia8_2018':{
    'xsec':1099.,
    'tags':['qcd',2018],
    'dirs':['crab_QCD_Pt-300toInf_EMEnriched_TuneCP5_13TeV_pythia8_2018_v1'], 
    },
'DiPhotonJetsBox_M40_80-Sherpa_2016':{
    'xsec':299.3,
    'tags':['ggjets',2016],
    'dirs':['crab_DiPhotonJetsBox_M40_80-Sherpa_2016_v1']
    },
'DiPhotonJetsBox_M40_80-Sherpa_2017':{
    'xsec':299.3,
    'tags':['ggjets',2017],
    'dirs':['crab_DiPhotonJetsBox_M40_80-Sherpa_2017_v1']
    },
'DiPhotonJetsBox_M40_80-Sherpa_2018':{
    'xsec':299.3,
    'tags':['ggjets',2018],
    'dirs':['crab_DiPhotonJetsBox_M40_80-Sherpa_2018_v1']
    },
'DiPhotonJetsBox_MGG-80toInf_13TeV-Sherpa_2016':{
    'xsec':84.0,
    'tags':['ggjets',2016],
    'dirs':['crab_DiPhotonJetsBox_MGG-80toInf_13TeV-Sherpa_2016_v1']
    },
'DiPhotonJetsBox_MGG-80toInf_13TeV-Sherpa_2017':{
    'xsec':84.0,
    'tags':['ggjets',2017],
    'dirs':['crab_DiPhotonJetsBox_MGG-80toInf_13TeV-Sherpa_2017_v1']
    },
'DiPhotonJetsBox_MGG-80toInf_13TeV-Sherpa_2018':{
    'xsec':84.0,
    'tags':['ggjets',2018],
    'dirs':['crab_DiPhotonJetsBox_MGG-80toInf_13TeV-Sherpa_2018_v1']
    },   
'SinglePhoton_2016':{
    'xsec':-1,
    'tags':['data',2016],
    'dirs':['crab_SinglePhoton_2016_{}'.format(x) for x in ['B_v1','B_v2','C_v1','D_v1','E_v1','F_v1','G_v1','H_v1']], 
    },  
'SinglePhoton_2017BCDE':{
    'xsec':-1,
    'tags':['data',2017],
    'note':'VBF trigger is not available (process separately with RDataFrame)',
    'dirs':['crab_SinglePhoton_2017_{}'.format(x) for x in ['B_v1','C_v1','D_v1','E_v1']], 
    }, 
'SinglePhoton_2017F':{
    'xsec':-1,
    'tags':['data',2017],
    'note':'VBF trigger is only present in this era (process separately with RDataFrame)',
    'dirs':['crab_SinglePhoton_2017_F_v1'], 
    }, 
'EGamma_2018':{
    'xsec':-1,
    'tags':['data',2018],
    'dirs':['crab_EGamma_2018_{}'.format(x) for x in ['A_v1','B_v1','C_v1','D_v1']], 
    },
}


#titles for the samples
_samplesTitles={
    'gjetslo'        : r'QCD $\gamma$+jets (LO)',
    'gjetslo40to100' : r'QCD $\gamma$+jets 40$<\hat{p}_T/GeV<$100 (LO)',
    'gjetslo100to200': r'QCD $\gamma$+jets 100$<\hat{p}_T/GeV<$200 (LO)',
    'gjetslo200to400': r'QCD $\gamma$+jets 200$<\hat{p}_T/GeV<$400 (LO)',
    'gjetslo400to600': r'QCD $\gamma$+jets 400$<\hat{p}_T/GeV<$600 (LO)',
    'gjetslo600toInf': r'QCD $\gamma$+jets $\hat{p}_T/GeV>$600 (LO)',
    'qcd'            : r'Multijets',
    'ggjets'         : r'$\gamma\gamma$+jets',    
    'ewgjjhw'        : r'EW $\gamma$jj (HW)',
    'ewgjjpy8'       : r'EW $\gamma$jj (PY8)',
    'data'           : r'Data'
}


def getSamplesMatchingTags(tags_list,samples_dict=_samples_Run2):
    
    """returns the samples (dirs) which match all the tags in tags_list"""
    
    chunks_list=[]
    
    #loop over samples
    for s,s_def in samples_dict.items():

        #require all tags to be found
        allFound=True
        for t in tags_list:
            if t in s_def['tags']: continue
            allFound=False
        if not allFound : continue
        chunks_list += s_def['dirs']
    
    return chunks_list
    
    
    
            
def collectAvailableSampleDirectories(input_dir,tags_list,samples_dict,justList=True):
    
    """returns a list of directories to process matching the tags in tags_list"""
        
    #check which samples are to be kept and make a dict: (dir,sample_name)
    sample_dirs={}
    for s,s_def in samples_dict.items():

        keep=False
        for t in s_def['tags']:
            if not t in tags_list: continue
            keep=True
            break
        if not keep: continue
    
        for d in s_def['dirs']: 
            sample_dirs[d]=s
    
    #now loop over the sub-directories and check which ones are available
    import os
    available_sample_dirs={}
    for dataset in os.listdir(input_dir):
        for sample in os.listdir(os.path.join(input_dir,dataset)):
            if not sample in sample_dirs: continue
            available_sample_dirs[sample]={'dir':os.path.join(input_dir,dataset,sample),
                                           'sample':sample_dirs[sample]}
            
    if justList:
        return [x['dir'] for _,x in available_sample_dirs.items()]
    else:
        return available_sample_dirs

 