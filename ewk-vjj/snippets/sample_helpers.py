#!/usr/bin/env python

"""
This file defines a series of generic helper methods to analyze the samples
"""

import os
import pickle
import ROOT
from collections import defaultdict

def collectFileInputs(inputs): 

    """
    walks through sub-directories and collects the root files per sample
    inputs is a directory or a list of directories
    the method assumes a crab-like output, i.e. for each directory it will find s.th. like directory/sample/.../*.root
    sample will be used as the key of the dict returned
    """
    
    #convert to a list if the input is a string
    if not isinstance(inputs,list): inputs=[inputs]

    #collect all files under each one of the inputs
    sample_list=defaultdict(list) 
    for path in inputs:
        sample=os.path.basename(path)
        for dp, dn, filenames in os.walk(path):
            for f in filenames:
                if os.path.splitext(f)[1] != '.root':
                    continue
                sample_list[sample].append(os.path.join(dp, f))

    return sample_list


def produceNormalizationCache(samples_list,outdir=None):

    """
    loops over the original NANOAOD files and collects the sum of weights from each one
    the arguments are a list of samples (output of the collectFileInputs method) and an output directory (outdir)
    If the later is specified, the weights are stored in a pickle file 
    returns the dict of weight sums
    """
    
    wgtSums={}
    
    #loop over samples
    for k,flist in samples_list.items():
        wgtSums[k]=[]
        
        #loop over files in this sample
        for f in flist:
            inF=ROOT.TFile.Open(f)
            h=inF.Get('wgtSum')
            
            #add this file sum of weights to the current sum of weights
            #if not yet available start the counters at 0.
            nbinsx=h.GetNbinsX()
            if len(wgtSums[k])==0:
                wgtSums[k]=[0.]*nbinsx
            for i in range(nbinsx):
                wgtSums[k][i]=wgtSums[k][i]+h.GetBinContent(i+1)
                
            inF.Close()
    
    if outdir:
        #pickle weights computed
        with open(os.path.join(outdir,'norm_cache.pck'),'wb') as fout:
            pickle.dump(wgtSums,fout,pickle.HIGHEST_PROTOCOL)
              
    return wgtSums


def mergeSnapshotChunksToDataFrames(inputs,outputDir,convertToPandas=False,force=True):

    """
    merges DataFrame chunks in inputs (can be a list of files or a directory)
    the  .h5 files will be grouped by name (assumes they are named as tag_name_{:d}.h5)
    the dataFrames are read from the .h5 files corresponding to the same tag_name and concatenated
    the output merged dataframes are placed in outputDir
    """
    
    import pandas as pd
    import re
    
    rgx=re.compile('(.*)_(.*)_(.*).root')
    
    if convertToPandas:             
        ROOT.ROOT.EnableImplicitMT()

    #group the snapshot chunks
    toMerge=defaultdict(list) 
    if os.path.isdir(inputs):
        for f in os.listdir(inputs):
            if not '.root' in f : continue
            key,start,end=rgx.match(f).groups()
            toMerge[key].append(os.path.abspath( os.path.join(inputs,f) ) )
    print('I have identified {} samples to merge'.format(len(toMerge)))
            
    #concatenate the dataframes and save the result
    os.system('mkdir -p %s'%outputDir)
    for key,flist in toMerge.items():

        print('Merging {} from {} chunks'.format(key,len(flist)))
        
        #hadd the chunks
        if not convertToPandas:
            outf='{}/{}.root'.format(outputDir,key)
            if os.path.isfile(outf) and not force: continue
            os.system('hadd -f -k {} {}'.format(outf,' '.join(flist)))
        else:
            outf='{}/{}.h5'.format(outputDir,key)
            if os.path.isfile(outf) and not force: continue
            try:
                files = ROOT.vector('string')()
                for f in flist: files.push_back(f)
                rdf=ROOT.RDataFrame('data',files)
                columns=[str(c) for c in rdf.GetColumnNames()]
                df=pd.DataFrame(rdf.AsNumpy(),columns=columns)
                df.to_hdf(outf,key='data')
            except Exception as e:
                print('Failed to create',outf)
                print('Exception',e)

    if not convertToPandas:
        ROOT.ROOT.DisableImplicitMT()

        
def main(): 

    import time
    for convertToPandas in [True,False]:
        start_time = time.time()
        mergeDataFrameChunks(inputs='/eos/user/p/psilva/data/ewk-vjj/tight/Chunks/',
                             outputDir='/eos/user/p/psilva/data/ewk-vjj/tight/Merged/',
                             convertToPandas=convertToPandas)
        end_time = time.time()
        print('Time elapsed:',end_time-start_time)


if __name__ == "__main__":
    main()
