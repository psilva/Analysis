import ROOT
import numpy as np
import os
import re

class NonPromptPhotonFitter:
    
    def __init__(self,outdir,verbose=False):
        
        """prepares some variables"""
        
        self.garbage=[] #garbage collector for ROOT stuff
        
        self.outdir=outdir #where to save the workspaces and plots
        os.system('mkdir -p {}'.format(self.outdir))
        
        #load an auxiliary macro to map the data in categories
        ROOT.gROOT.LoadMacro('snippets/MappedRooDataHist.h+')
        if not verbose: ROOT.shushRooFit() #keep RooFit quiet
      
    def runFit(self,hists_dict,fitName,physModel='linear',ncpu=2):
        
        """
        starts the workspace
        hists_dict is a list of dicts with histograms (numpy arrays) for each slice 
        which is expected be filled as follows
        [{ 'avg':float, 'prompt_loose':histo, 'prompt_tight':histo, 'data_loose':histo, 'data_tight':histo}, ... ]
        fitName it's just a name for the final workspace file
        
        physModel is the physics model to impose on the prompt kfactor
        * linear - a*pT+b
        * expo   - a*exp(-b*pT)
        * ratio  - b/(1+a*pT)
        in all cases b=b0+theta * sigma_b0 where theta is a gaussian nuisance
        """
        
        #start a workspace
        ws=ROOT.RooWorkspace("w")
        
        #define the observable
        nbins=len(hists_dict[0]['data_tight'])
        ws.factory('x[0,0,{}]'.format(nbins))       
        obsSet=ROOT.RooArgSet(ws.var('x'))
        
        #slicing observable and category names
        ws.factory('y[0,1e9]')
        n_categories=len(hists_dict)
        category_names=['cat{}'.format(i) for i in range(n_categories)]

        #physics model for the scaling factor of the prompt component (k0+theta*sigma_k) * exp(-lambda * y )
        #this will be specified in each category such that the y value corresponds to the one in that category
        #k0 is initially estimated from total yields in bin 0 tight         
        total_data_tight_bin0   = sum( [ category['data_tight'][0] for category in hists_dict ] )
        total_prompt_tight_bin0 = sum( [ category['prompt_tight'][0] for category in hists_dict ] )
        kfact0                  = total_data_tight_bin0/total_prompt_tight_bin0
        sigma_kfact0            = abs(1-kfact0)
        print('Initial estimate for prompt k-fact is: {:3.3f} +/- {:3.3f}'.format(kfact0,sigma_kfact0))   
        
        if physModel=='linear':
            ws.factory("RooFormulaVar::kfact('(@0+@1*@2)+@3*@4',{{"
                       "kfact0[{}],theta_kfact[0,-5,5],sigma_kfact0[{}],"
                       "lambdak[0,-1,1],y"
                       "}})".format(kfact0,sigma_kfact0) )
        elif physModel=='expo':
            ws.factory("RooFormulaVar::kfact('(@0+@1*@2)*exp(-@3*@4)',{{"
                       "kfact0[{}],theta_kfact[0,-5,5],sigma_kfact0[{}],"
                       "lambdak[0,-1,1],y"
                       "}})".format(kfact0,sigma_kfact0) )
        else:
            ws.factory("RooFormulaVar::kfact('(@0+@1*@2)/(1+@3*@4)',{{"
                       "kfact0[{}],theta_kfact[0,-5,5],sigma_kfact0[{}],"
                       "lambdak[0,-1,1],y"
                       "}})".format(kfact0,sigma_kfact0) )
    
        #gaussian constraint for the nuisance on k0 to adjust initial estimate further during the fit step
        ws.factory('Gaussian::gauss_kfact(theta_kfact,0.,1.)')
        
        #import histograms and define PDFs in the RooWorkspace 
        #(needs TH1F -> RooDataHist conversion first)
        #store also the yields (first bin and total per categories)
        yields={}
        dataMap=ROOT.MappedRooDataHist()
        for icat in range(n_categories):
            
            category=hists_dict[icat]
            yval=category['avg']
            for hname in ['data_loose','data_tight','prompt_loose','prompt_tight']:
                
                htag='{}_cat{}'.format(hname,icat)
                
                #import histogram and save yields
                rh=self.fillROOTHisto(category[hname],htag)
                getattr(ws,'import')(ROOT.RooDataHist(htag,htag,ROOT.RooArgList(obsSet),rh))
                
                yields[(hname,icat)]={'total':rh.Integral(),'0':rh.GetBinContent(1)}
            
                #normalize and import as PDF for prompt and control region in data
                if hname !='data_tight':
                    rh.Scale(1./yields[(hname,icat)]['total'])
                    for xbin in range(2,rh.GetNbinsX()+1):
                        if rh.GetBinContent(xbin)>0 : continue
                        rh.SetBinContent( xbin,1e-6 )
                    getattr(ws,'import')(
                        ROOT.RooDataHist('norm_'+htag,
                                         'norm_'+htag,
                                         ROOT.RooArgList(obsSet),
                                         rh) 
                    )
                    getattr(ws,'import')(
                        ROOT.RooHistPdf('pdf_{}'.format(htag),
                                        'pdf_{}'.format(htag),
                                        obsSet,
                                        ws.data('norm_'+htag))
                    )
      
                #free memory
                rh.Delete()
            
                #in each slice 
                # *the data will correspond to a fixed value of y and stored in a binned map
                # *the prompt will be scaled with the k-factor formula of the physics model
                #  fixing the slicing variable y : we do this by adapting the formula and fixing y

                if hname=='data_tight': 
                    
                    ws.factory('y_cat{}[{}]'.format(icat,yval))
                    ws.var('y_cat{}'.format(icat)).setConstant(True)
                    binnedData=ws.data(htag)
                    dataMap.add('cat{}'.format(icat),binnedData)
                    
                    ws.factory("EDIT::kfact_cat{0}(kfact,y=y_cat{0})".format(icat))
                    
                if 'prompt' in hname:                
                    nexp_prompt=yields[(hname,icat)]['total']
                    ws.factory('Nexp_{}[{}]'.format(htag,nexp_prompt))
                    ws.factory("RooFormulaVar::N_{0}( '@0*@1', {{ Nexp_{0}, kfact_cat{1} }} )".format(htag,icat))
                    
            #create generic PDF for nonprompt in this category
            #data(loose) - k* prompt(loose)
            ndata_loose = yields[('data_loose',icat)]['total']
            ws.factory('N_data_loose_cat{}[{}]'.format(icat,ndata_loose))
            ws.factory("EXPR::pdf_nonprompt_cat{0}('"
                            "@0*@1>@2*@3 ? @0*@1-@2*@3 : 0.',"
                            "{{ N_data_loose_cat{0}, pdf_data_loose_cat{0}, N_prompt_loose_cat{0}, pdf_prompt_loose_cat{0} }}"
                            ")".format(icat) )
            
            #create final fit model for single category:  k*Prompt + Nonprompt
            ndata_tight=yields[('data_tight',icat)]['total']
            nexp_tight=yields[('prompt_tight',icat)]['total']
            nnonprompt_tight=max(ndata_tight-nexp_tight,0.) #initial estimate
            ws.factory('N_nonprompt_tight_cat{}[{},0,{}]'.format(icat,nnonprompt_tight,1.2*ndata_tight))
            ws.factory("SUM::model_cat{0}("
                            "N_prompt_tight_cat{0}*pdf_prompt_tight_cat{0},"
                            "N_nonprompt_tight_cat{0}*pdf_nonprompt_cat{0})".format(icat) )
                                    
        #after creating the PDFs for the individual categories, join them in a simulatenous model
        #and multiply by the gaussian constraints
        ycats='ycats[{}]'.format( ','.join(category_names) )
        ypdfs=','.join( ['cat{0}=model_cat{0}'.format(i) for i in range(n_categories)] )
        ws.factory('SIMUL::simmodel({},{})'.format(ycats,ypdfs))
        ws.factory("PROD::model( simmodel, gauss_kfact )")
         
        #build a combined data histogram for several categories
        getattr(ws,'import')( ROOT.RooDataHist("data","data",
                                                    ROOT.RooArgList(obsSet),
                                                    ws.cat('ycats'),
                                                    dataMap.get()) )
                   
        #debug
        #ws.Print('v')

        self.garbage.append(ws)
        
        #perform the fit
        pdf=ws.pdf('model')
        fit_params = pdf.getParameters(ws.var('x'));        
        data=ws.data('data')
        fitResult=pdf.fitTo(data,ROOT.RooFit.Extended(),ROOT.RooFit.Save(True),ROOT.RooFit.NumCPU(ncpu))
        ws.saveSnapshot("fitresult_params", fit_params, True)
        getattr(ws,'import')( fitResult )
        
        #save to file
        fit_file=os.path.join(self.outdir,'{}.root'.format(fitName))
        ws.writeToFile( fit_file )
        print('Workspace and fit results have been stored in',fit_file)
        
        return fit_file
    
    def getPOIs(self,url):
        
        """
        returns the parameters of interest of the fit
        notice these are POIs in the strict sense of the fit but rather the parameters which 
        are most relevant for the analysis:
        iso-loose / iso-tight ratios
        purity
        k-factor
        the results are returned in a dict { var : [ bins, values, values_unc] }
        """
        
        #get the workspace and load the fit results
        fIn=ROOT.TFile.Open(url)
        w=fIn.Get('w')
        w.loadSnapshot('fitresult_params')
        fit_result=w.genobj('fitresult_model_data')
        
        #fitted yields in the first bin
        w.var('x').setVal(0)
        obs0 = ROOT.RooArgSet(w.var('x')) 
        
        #categories to project
        ycats=w.cat('ycats')
        category_list=[s.first for s in ycats.states()]

        #loop over categories and get POIs
        y = []
        k_prompt,k_prompt_unc = [],[]
        epsIL, epsIL_unc = [],[]
        epsIT, epsIT_unc = [],[]
        purity, purity_unc = [],[]
        for category in category_list:
            
            y.append(w.var('y_{}'.format(category)).getVal())
            
            #prompt scaling factor
            kfact=w.function('kfact_{}'.format(category))
            k_prompt.append( kfact.getVal() )
            k_prompt_unc.append( kfact.getPropagatedError(fit_result) )
            
            #nonprompt scaling parameters
            n_nonprompt_tight     = w.function('N_nonprompt_tight_{}'.format(category))
            n_nonprompt_tight_val = n_nonprompt_tight.getVal()
            n_nonprompt_tight_unc = n_nonprompt_tight.getPropagatedError(fit_result)
            p_nonprompt_tight     = w.pdf('pdf_nonprompt_{}'.format(category)).getVal(obs0)
           
            #iso-to-loose ratio for nonprompt
            n_obs_loose = w.data('data_loose_{}'.format(category)).sumEntries()
            epsIL.append( n_nonprompt_tight_val*p_nonprompt_tight/n_obs_loose )
            epsIL_unc.append( epsIL[-1]*ROOT.TMath.Sqrt( (n_nonprompt_tight_unc/n_nonprompt_tight_val)**2 + 
                                                          1./n_obs_loose ) )
            
            #tight-to-loose ratio for nonprompt
            n_obs_tight = w.data('data_tight_{}'.format(category)).sumEntries()
            epsIT.append( n_nonprompt_tight_val*p_nonprompt_tight/n_obs_tight )
            epsIT_unc.append( epsIT[-1]*ROOT.TMath.Sqrt( (n_nonprompt_tight_unc/n_nonprompt_tight_val)**2 +
                                                          1./n_obs_tight ) )

            #purity
            w.data('data_tight_{}'.format(category)).get(0)
            n_obs_tight0 = w.data('data_tight_{}'.format(category)).weight()
            n_prompt_tight     = w.function('N_prompt_tight_{}'.format(category))
            n_prompt_tight_val = n_prompt_tight.getVal()
            n_prompt_tight_unc = n_prompt_tight.getPropagatedError(fit_result)
            p_prompt_tight     = w.pdf('pdf_prompt_tight_{}'.format(category)).getVal(obs0)

            total_tight=n_nonprompt_tight_val*p_nonprompt_tight+n_prompt_tight_val*p_prompt_tight
            
            purity.append( (n_prompt_tight_val*p_prompt_tight)/total_tight )
            purity_unc.append( purity[-1]*ROOT.TMath.Sqrt( (n_prompt_tight_unc/n_prompt_tight_val)**2+
                                                           1./total_tight ) )
                              
        #all done with this ROOT file
        fIn.Close()

        y=np.array(y)
        pois={
            'kfact'  : [y,np.array(k_prompt),np.array(k_prompt_unc)],
            'epsIL'  : [y,np.array(epsIL),np.array(epsIL_unc)],
            'epsIT'  : [y,np.array(epsIT),np.array(epsIT_unc)],
            'purity' : [y,np.array(purity),np.array(purity_unc)]
        }
        return pois        
        
        
    def showFitResult(self,url,title,outname,
                      xlabel='I_{ch} (bin number)', ylabel='Events'):
        
        """shows the canvas with the final values of the fit used for the PDF"""

        try:
            from rounding import toROOTRounded
        except:
            from snippets.rounding import toROOTRounded

        #start the canvas
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptTitle(0)
        
        c = ROOT.TCanvas('c','c',500,500)
        self.garbage.append(c)
        
        p1 = ROOT.TPad('p1','p1',0.0,0.75,1.0,0.0)
        p1.Draw()
        p1.cd()
        p1.SetRightMargin(0.05)
        p1.SetLeftMargin(0.12)
        p1.SetTopMargin(0.008)
        p1.SetBottomMargin(0.2)
        p1.SetGridx(True)

        c.cd()
        p2 = ROOT.TPad('p2','p2',0.0,0.75,1.0,1.0)
        p2.Draw()
        p2.cd()
        p2.SetBottomMargin(0.005)
        p2.SetRightMargin(0.05)
        p2.SetLeftMargin(0.12)
        p2.SetTopMargin(0.3)
        p2.SetGridx(True)
        p2.SetGridy(True)
        
        #get the parameters of interest to display
        pois=self.getPOIs(url)
        
        #read the workspace
        fIn=ROOT.TFile.Open(url)
        w=fIn.Get('w')
        w.loadSnapshot("fitresult_params")

        #categories to project
        ycats=w.cat('ycats')
        category_list=[s.first for s in ycats.states()]

        #loop over categories
        for category in category_list:

            #show data and PDF components in the main pad
            p1.cd()
            p1.Clear()
            frame=w.var('x').frame()
        
            #data
            data=w.data('data_tight_{}'.format(category))
            nbins=data.numEntries()
            data.plotOn(frame,ROOT.RooFit.Name('data'))
            ycats.setLabel(category)

            pdf=w.pdf('model_{}'.format(category)) 
            pdf.plotOn(frame,
                       ROOT.RooFit.Slice(ycats,category),
                       ROOT.RooFit.ProjWData(data),
                       ROOT.RooFit.Name('total'),
                       ROOT.RooFit.MoveToBack())
            
            #compute here the chi2 and the pull
            chi2=frame.chiSquare() 
            hpull = frame.pullHist()

            #show also nonprompt
            pdf.plotOn(frame,
                       ROOT.RooFit.ProjWData(data),
                       ROOT.RooFit.Slice(ycats,category),
                       ROOT.RooFit.Components('pdf_nonprompt_{}'.format(category)),
                       ROOT.RooFit.FillColor(15),
                       ROOT.RooFit.FillStyle(3004),
                       ROOT.RooFit.DrawOption("F"),
                       ROOT.RooFit.Name('nonprompt'),
                       ROOT.RooFit.MoveToBack())
 
            frame.Draw()
            p1.SetLogy()
            frame.GetYaxis().SetTitleOffset(1.0)
            frame.GetYaxis().SetTitleSize(0.06)
            frame.GetYaxis().SetLabelSize(0.05)
            frame.GetXaxis().SetTitleSize(0.06)
            frame.GetXaxis().SetLabelSize(0.05)
            frame.GetXaxis().SetTitle(xlabel)
            frame.GetYaxis().SetTitle(ylabel)
               
            #print the parameters of interest
            label = ROOT.TLatex()
            label.SetNDC()
            label.SetTextFont(42)
            label.SetTextSize(0.045)
            avg_y=w.var('y_{}'.format(category)).getVal()
            label.DrawLatex(0.6,0.92,'<p_{T}>=%3.1f'%avg_y)
            label.DrawLatex(0.6,0.86,'#chi^{2}/bins=%3.1f/%d'%(chi2,nbins))
            idx=int(re.findall(r'\d+',category)[0])
            for i, p in enumerate([ ('epsIL','#varepsilon_{IL}'),
                                    ('epsIT','#varepsilon_{IT}'),
                                    ('kfact','k(prompt)'),
                                    ('purity','purity') ]):
                
                pname,ptitle=p
                try:
                    label.DrawLatex(0.6,0.80-0.06*i,'{}={}'.format(ptitle,
                                                                   toROOTRounded(pois[pname][1][idx],pois[pname][2][idx]) 
                                                                  )
                                   )
                except Exception as e:
                    print('Error printing {} : {}'.format(pname,e))

            leg = ROOT.TLegend(0.2,0.95,0.5,0.75)
            leg.SetFillStyle(0)
            leg.SetBorderSize(0)
            leg.SetTextFont(42)
            leg.SetTextSize(0.045)
            leg.AddEntry('data','Data', 'eP');        
            leg.AddEntry('total','Prompt','l')
            leg.AddEntry('nonprompt','Nonprompt','f')
            leg.Draw()
            self.garbage.append(leg)
        
            #draw the pull in the second pad
            p2.cd()
            p2.Clear()
            pullFrame = w.var('x').frame()
            pullFrame.addPlotable(hpull,"P") ;
            pullFrame.Draw()
            pullFrame.GetYaxis().SetTitle("Pull")
            pullFrame.GetYaxis().SetTitleSize(0.2)
            pullFrame.GetYaxis().SetLabelSize(0.15)
            pullFrame.GetXaxis().SetTitleSize(0)
            pullFrame.GetXaxis().SetLabelSize(0)
            pullFrame.GetYaxis().SetTitleOffset(0.15)
            pullFrame.GetYaxis().SetNdivisions(4)
            pullFrame.GetYaxis().SetRangeUser(-5.4,5.4)
            pullFrame.GetXaxis().SetTitleOffset(0.8)
        
            label2 = ROOT.TLatex()
            label2.SetNDC()
            label2.SetTextFont(42)
            label2.SetTextSize(0.2)
            label2.DrawLatex(0.12,0.8,'#bf{CMS} #it{preliminary}')
            label2.SetTextSize(0.15)
            label2.SetTextAlign(ROOT.kVAlignCenter+ROOT.kHAlignRight)
            label2.DrawLatex(0.96,0.8,title)
            c.Modified()
            c.Update()
        
            #save canvas
            for ext in ['png','pdf']:
                c.SaveAs('{}_{}.{}'.format(outname,category,ext))
                
        #all done with this file
        fIn.Close()

        
    def fillROOTHisto(self,h,hname,hunc=None):
        
        """just a dummy function to convert an array to an histogram"""

        n=len(h)
        rh=ROOT.TH1F(hname,hname,n,0,n)
        for i in range(n):
            rh.SetBinContent(i+1,h[i])
            if hunc is None: continue
            rh.SetBinError(i+1,hunc[i])
        return rh
        
    def flush(self):
        for o in self.garbage: o.Delete()
            
          
def main():
    
    import os
    import numpy as np
    import pandas as pd
    import pickle

    histos_indir='/eos/user/p/psilva/data/ewk-vjj/tight/Histograms/2016/'
    cat='lpt'
    
    #read histograms and build the map of slices
    slices = []
    samples  =['gjetslo_prompt','data']
    for s in samples:
        
        proc='prompt' if 'prompt' in s else 'data'
        with open('{}/{}.pck'.format(histos_indir,s),'rb') as fin:
            histos=pickle.load(fin)
        
        loose=histos['{}_loose_chisovspt'.format(cat)]['histo']
        tight=histos['{}_tight_chisovspt'.format(cat)]['histo']
        
        bins=loose[0][0]
        nbins=len(bins)-1    
        for i in range( nbins ):
            if len(slices)<=i : slices.append({})
            ptmin,ptmax=bins[i:i+2]
            slices[i]['{}_loose'.format(proc)]=loose[1][i,:]
            slices[i]['{}_tight'.format(proc)]=tight[1][i,:]
            
        #fill the values for the average pT observed
        #which will be used as "slicing" variabble
        #in the multi-category fit
        if s!='data':continue
        ptprofile=histos['{}_tight_ptprofile'.format(cat)]['histo'][1]
        for i in range( nbins ):
            slices[i]['avg'] = ptprofile[i]
                        
    #call the template fitter to extract the tight-to-loose ratio
    nppf=NonPromptPhotonFitter('/eos/user/p/psilva/data/ewk-vjj/test/')
    ws_url=nppf.runFit(hists_dict=slices,fitName='testnpfit')
    nppf.showFitResult(ws_url,'test','testnpfit')


if __name__ == "__main__":
    main()
    