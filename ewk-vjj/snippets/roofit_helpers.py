import ROOT
import numpy as np
import os
import re

class MyRooFitter:
    
    def __init__(self,outdir,verbose=False):
        
        """prepares some variables"""
        
        self.garbage=[] #garbage collector for ROOT stuff
        
        self.outdir=outdir #where to save the workspaces and plots
        os.system('mkdir -p {}'.format(self.outdir))
        
        #load an auxiliary macro to map the data in categories
        ROOT.gROOT.LoadMacro('snippets/MappedRooDataHist.h+')
        if not verbose: ROOT.shushRooFit() #keep RooFit quiet
            
        self.verbose=verbose
            
    def runFit(self,hists_dict,fitName,ncpu=2):
        pass
    
    def getPOIs(self,url):
        pass
        
    def showFitResult(self,
                      url,
                      title, 
                      outname,
                      fit_results_snapshot="fitresult_params",
                      data_name='data_tight',
                      model_name='model',
                      model_title='Prompt',
                      components_list=['pdf_nonprompt'],
                      components_titles=['nonprompt'],
                      xlabel='I_{ch} bin number',
                      ylabel='Events',
                      slicevar_title='p_{T}',
                      params_caption_handle=None                    
                     ):
        
        """shows the canvas with the final values of the fit used for the PDF"""

        try:
            from rounding import toROOTRounded
        except:
            from snippets.rounding import toROOTRounded

        #start the canvas
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptTitle(0)
        
        
        color_list=['#2c7bb6','#abd9e9','#fdae61','#ffffbf','#d7191c']

        
        c = ROOT.TCanvas('c','c',500,500)
        self.garbage.append(c)
        
        p1 = ROOT.TPad('p1','p1',0.0,0.75,1.0,0.0)
        p1.Draw()
        p1.cd()
        p1.SetRightMargin(0.05)
        p1.SetLeftMargin(0.12)
        p1.SetTopMargin(0.008)
        p1.SetBottomMargin(0.2)
        p1.SetGridx(True)

        c.cd()
        p2 = ROOT.TPad('p2','p2',0.0,0.75,1.0,1.0)
        p2.Draw()
        p2.cd()
        p2.SetBottomMargin(0.005)
        p2.SetRightMargin(0.05)
        p2.SetLeftMargin(0.12)
        p2.SetTopMargin(0.3)
        p2.SetGridx(True)
        p2.SetGridy(True)
             
        #read the workspace
        fIn=ROOT.TFile.Open(url)
        w=fIn.Get('w')
        w.loadSnapshot(fit_results_snapshot)

        #categories to project
        ycats=w.cat('ycats')
        category_list=[s.first for s in ycats.states()]

        #loop over categories
        for category in category_list:

            #show data and PDF components in the main pad
            p1.cd()
            p1.Clear()
            frame=w.var('x').frame()
        
            #data
            data=w.data('{}_{}'.format(data_name,category))
            nbins=data.numEntries()
            data.plotOn(frame,ROOT.RooFit.Name('data'))
            ycats.setLabel(category)

            pdf=w.pdf('{}_{}'.format(model_name,category)) 
            pdf.plotOn(frame,
                       ROOT.RooFit.Slice(ycats,category),
                       ROOT.RooFit.ProjWData(data),
                       ROOT.RooFit.LineColor( ROOT.TColor.GetColor(color_list[0]) ),
                       ROOT.RooFit.Name('total'),
                       ROOT.RooFit.MoveToBack())
            
            #compute here the chi2 and the pull
            chi2=frame.chiSquare() 
            hpull = frame.pullHist()

            #show also sub-components
            for ic,component in enumerate(components_list):
                print(ic,component,component.format(ic))
                pdf.plotOn(frame,
                           ROOT.RooFit.ProjWData(data),
                           ROOT.RooFit.Slice(ycats,category),
                           ROOT.RooFit.Components(component.format(category)),
                           ROOT.RooFit.FillColor( ROOT.TColor.GetColor(color_list[ic+1]) ),
                           ROOT.RooFit.FillStyle(1001),
                           ROOT.RooFit.DrawOption("F"),
                           ROOT.RooFit.Name('component_{}'.format(ic)),
                           ROOT.RooFit.MoveToBack())
 
            frame.Draw()
            p1.SetLogy()
            frame.GetYaxis().SetTitleOffset(1.0)
            frame.GetYaxis().SetTitleSize(0.06)
            frame.GetYaxis().SetLabelSize(0.05)
            frame.GetXaxis().SetTitleSize(0.06)
            frame.GetXaxis().SetLabelSize(0.05)
            frame.GetXaxis().SetTitle(xlabel)
            frame.GetYaxis().SetTitle(ylabel)
               
            #print the parameters of interest
            label = ROOT.TLatex()
            label.SetNDC()
            label.SetTextFont(42)
            label.SetTextSize(0.045)
            avg_y=w.var('y_{}'.format(category)).getVal()
            label.DrawLatex(0.6,0.92,'<%s>=%3.1f'%(slicevar_title,avg_y))
            label.DrawLatex(0.6,0.86,'#chi^{2}/bins=%3.1f/%d'%(chi2,nbins))
            if params_caption_handle:
                params_caption_handle(w,idx,label,(0.6,0.80))

            leg = ROOT.TLegend(0.2,0.95,0.5,0.75)
            leg.SetFillStyle(0)
            leg.SetBorderSize(0)
            leg.SetTextFont(42)
            leg.SetTextSize(0.045)
            leg.AddEntry('data','Data', 'eP');        
            leg.AddEntry('total',model_title,'l')
            for ic,component_title in enumerate(components_titles): 
                leg.AddEntry('component_{}'.format(ic),component_title,'f')
            leg.Draw()
            self.garbage.append(leg)
        
            #draw the pull in the second pad
            p2.cd()
            p2.Clear()
            pullFrame = w.var('x').frame()
            pullFrame.addPlotable(hpull,"P") ;
            pullFrame.Draw()
            pullFrame.GetYaxis().SetTitle("Pull")
            pullFrame.GetYaxis().SetTitleSize(0.2)
            pullFrame.GetYaxis().SetLabelSize(0.15)
            pullFrame.GetXaxis().SetTitleSize(0)
            pullFrame.GetXaxis().SetLabelSize(0)
            pullFrame.GetYaxis().SetTitleOffset(0.15)
            pullFrame.GetYaxis().SetNdivisions(4)
            pullFrame.GetYaxis().SetRangeUser(-5.4,5.4)
            pullFrame.GetXaxis().SetTitleOffset(0.8)
        
            label2 = ROOT.TLatex()
            label2.SetNDC()
            label2.SetTextFont(42)
            label2.SetTextSize(0.2)
            label2.DrawLatex(0.12,0.8,'#bf{CMS} #it{preliminary}')
            label2.SetTextSize(0.15)
            label2.SetTextAlign(ROOT.kVAlignCenter+ROOT.kHAlignRight)
            label2.DrawLatex(0.96,0.8,title)
            c.Modified()
            c.Update()
        
            #save canvas
            for ext in ['png','pdf']:
                c.SaveAs('{}_{}.{}'.format(outname,category,ext))
                
        #all done with this file
        fIn.Close()

        
    def fillROOTHisto(self,h,hname,hunc=None):
        
        """just a dummy function to convert an array to an histogram"""

        n=len(h)
        rh=ROOT.TH1F(hname,hname,n,0,n)
        for i in range(n):
            rh.SetBinContent(i+1,h[i])
            if hunc is None: continue
            rh.SetBinError(i+1,hunc[i])
        return rh
        
    def flush(self):
        for o in self.garbage: o.Delete()