import numpy as np
import pandas as pd
import os
import ROOT
import PyRDF
import copy

def getEWVJJTriggersFor(era,isMC):
    
    """ returns the trigger list for a given era """
    
    trigs=[]
    if '2016' in era:
        trigs=['HLT_Photon75_R9Id90_HE10_Iso40_EBOnly_VBF',
               'HLT_Photon50',
               'HLT_Photon50_R9Id90_HE10_IsoM',
               'HLT_Photon75_R9Id90_HE10_IsoM',
               'HLT_Photon175',                            
               'HLT_Photon90']
    if '2017' in era:
        trigs=['HLT_Photon200',
               'HLT_Photon150',
               'HLT_Photon90_R9Id90_HE10_IsoM',
               'HLT_Photon75_R9Id90_HE10_IsoM']
        if '2017_F' in era or isMC:
            trigs += ['HLT_Photon75_R9Id90_HE10_IsoM_EBOnly_PFJetsMJJ300DEta3',
                      'HLT_Photon50_R9Id90_HE10_IsoM']
    if '2018' in era:
        trigs=['HLT_Photon75_R9Id90_HE10_IsoM_EBOnly_PFJetsMJJ300DEta3',
               'HLT_Photon50',
               'HLT_Photon50_R9Id90_HE10_IsoM',
               'HLT_Photon200',
               'HLT_Photon90']
        
    return trigs


def defineEWVJJSelectionAndBranches(args):

    """
    wraps the procedure of selecting the events and collecting the necessary info in a small dataframe
    returns a pandas DataFrame
    """

    #if output has been produced skip this task
    outdir   = args['outdir']
    era      = args['era']
    force    = args['force']    
    out_file = '{}/Chunks/{}.root'.format(outdir,era)
    if os.path.isfile(out_file) and not force:
        return True

    #parse the arguments
    files    = args['files']
    isData   = args['isData']
    local    = args['local'] if 'local' in args else False
    
    if local:
        ROOT.ROOT.EnableImplicitMT()
        PyRDF.use('local')
    else:
        PyRDF.use('spark', {'npartitions':20})
        files = ['root://eosuser.cern.ch/{}'.format(x) if x.find('root:')<0 else x for x in files]
        out_file='root://eosuser.cern.ch/{}'.format(out_file)

    PyRDF.include_headers("snippets/nanoaod_helpers.h")
    
    #define the selections and branches with an RDataFrame
    rdf = PyRDF.RDataFrame("Events", files)

    branchList=['run','event','PV_npvsGood','fixedGridRhoFastjetAll']
    if not isData:
        branchList += ['genWeight']
        branchList += ['vjj_photon_effWgt' + x for x in ['','Up','Dn']]
        branchList += ['puWeight'+x for x in ['','Up','Down']]
        if not '2018' in era: 
            branchList += ['PrefireWeight'+x for x in ['','_Up','_Down']]
        
    #
    # RECO LEVEL selections
    #
        
    #trigger selection (require at least one fired)
    trigs=getEWVJJTriggersFor(era,False if isData else True)
    triggerBranches=[]
    for t in trigs:
        b='Pass_{}'.format(t)
        rdf=rdf.Define(b, 'int({}==1)'.format(t) )
        triggerBranches.append(b)
    trigFilt='+'.join(triggerBranches)+'>0'   
    branchList+=triggerBranches

    #photon selection
    good_photon = 'Photon_pt>50 && (Photon_isScEtaEB==1 || Photon_isScEtaEE==1)' #kinematics
    good_photon += '&& Photon_pixelSeed==0 && Photon_idcode>0'                   #id (idcode defined below)
    tight_photon = good_photon + ' && Photon_idcode>=256'                        #tight photons
    rdf = rdf.Define('Photon_idcode','getPhotonIDSummary(Photon_cutBased,Photon_vidNestedWPBitmap)') \
             .Define('good_photon',good_photon) \
             .Define('na','Sum(good_photon)')
    photonBranches=['pt','eta','phi','isScEtaEB','r9','sieie','pfRelIso03_chg','idcode']
    for attr in photonBranches:
        if attr=='isScEtaEB':
            rdf=rdf.Define(attr,'na>0 ? int(Photon_{}[good_photon][0]) : 0.'.format(attr))
        else:
            rdf=rdf.Define(attr,'na>0 ? Photon_{}[good_photon][0] : 0.'.format(attr))  
    rdf = rdf.Define('tight_photon',tight_photon) \
             .Define('ntight','Sum(tight_photon)')
    branchList += photonBranches + ['ntight']
    
    #jet selection
    good_jet  = 'Jet_pt>15 && abs(Jet_eta)<4.7'                                #kinematics
    good_jet += ' && (na==0 || crossClean(Jet_eta,Jet_phi,eta,phi))'           #cross clean to photon candidate
    if '2016' in era:
        good_jet += ' && Jet_jetId>0'                                          #loose id
    if '2017' in era:
        good_jet += ' && Jet_jetId>1'                                          #tight lep veto and ECAL noise       
        good_jet += ' && !(abs(Jet_eta)>2.650 && abs(Jet_eta)<3.139 && (Jet_chEmEF+Jet_neEmEF>0.55))'    
    if '2018' in era:
        good_jet += ' && Jet_jetId>1'                                          #tight lep veto and HEM 15/16 failure
        good_jet += ' && !(Jet_eta>-3.0 && Jet_eta<-1.3 && Jet_phi>-1.57 && Jet_phi<-0.87)'
        
    rdf = rdf.Define('good_jet',good_jet) \
             .Define('good_tag_jet','good_jet && Jet_pt>30') \
             .Define('nj','Sum(good_tag_jet)') \
             .Define('ptj1', 'nj>0 ? Jet_pt[good_tag_jet][0] : -9999.') \
             .Define('etaj1','nj>0 ? Jet_eta[good_tag_jet][0] : -9999.') \
             .Define('phij1','nj>0 ? Jet_phi[good_tag_jet][0] : -9999.') \
             .Define('mj1',  'nj>0 ? Jet_mass[good_tag_jet][0] : -9999.') \
             .Define('aj1',  'nj>0 ? Jet_area[good_tag_jet][0] : -9999.') \
             .Define('qglj1','nj>0 ? Jet_qgl[good_tag_jet][0] : -9999.') \
             .Define('puidj1','nj>0 ? int(Jet_puId[good_tag_jet][0]>6) : 0') \
             .Define('ptj2', 'nj>1 ? Jet_pt[good_tag_jet][1] : -9999.') \
             .Define('etaj2','nj>1 ? Jet_eta[good_tag_jet][1] : -9999.') \
             .Define('phij2','nj>1 ? Jet_phi[good_tag_jet][1] : -9999.') \
             .Define('mj2',  'nj>1 ? Jet_mass[good_tag_jet][1] : -9999.') \
             .Define('aj2',  'nj>1 ? Jet_area[good_tag_jet][1] : -9999.') \
             .Define('qglj2','nj>1 ? Jet_qgl[good_tag_jet][1] : -9999.') \
             .Define('puidj2','nj>1 ? int(Jet_puId[good_tag_jet][1]>6) : 0') \
             .Define('detajj','nj>1 ? Jet_eta[good_tag_jet][0]-Jet_eta[good_tag_jet][1]: -9999.') \
             .Define('mjj','nj>1 ? GetPairInvariantMass(Jet_pt[good_tag_jet], Jet_eta[good_tag_jet], Jet_phi[good_tag_jet], Jet_mass[good_tag_jet], 0, 1): -9999.') \
             .Define('balance','nj>1 ? GetBalance(pt,phi,ptj1,phij1,ptj2,phij2) : -9999.')
    branchList += ['nj','detajj','mjj','balance']              
    branchList += [x+y for x in ['pt','eta','phi','m','a','qgl','puid'] for y in ['j1','j2']]
    
    #central jet
    rdf = rdf.Define('central_jet','centralJet(Jet_pt[good_jet],Jet_eta[good_jet],Jet_phi[good_jet],etaj1,phij1,etaj2,phij2)') \
             .Define('ncenj','nj>2 ? Sum(central_jet) : 0.') \
             .Define('htcen','ncenj>0 ? Sum(Jet_pt[good_jet][central_jet]) : 0') \
             .Define('ptj3', 'ncenj>0 ? Jet_pt[good_jet][central_jet][0] : 0') \
             .Define('etaj3','ncenj>0 ? Jet_eta[good_jet][central_jet][0] : 0') \
             .Define('phij3','ncenj>0 ? Jet_phi[good_jet][central_jet][0] : 0') \
             .Define('mj3',  'ncenj>0 ? Jet_mass[good_jet][central_jet][0] : 0') \
             .Define('aj3',  'ncenj>0 ? Jet_area[good_jet][central_jet][0] : 0')
    branchList += [ 'ncenj','htcen','ptj3','etaj3','phij3','mj3','aj3' ]
 
    #skim
    skim='int('
    skim+= '({}) && na>0'.format(trigFilt)
    skim+= ' && (nj>1 || (nj==1 && puidj1>0))' #at least two jets or 1 good jet
    skim+=')'
    rdf=rdf.Define('pass_skim', skim)
    branchList += ['pass_skim']

    #
    # GEN LEVEL 
    #
    if not isData:
    
        #add MC truth for reconstructed objets
        #photons must be stable (status=1) and prompt (statusFlags=0)
        prompt_gen_photons='isPromptFinalState(22,GenPart_pdgId,GenPart_status,GenPart_statusFlags)'
        good_gen_photon  = 'GenPart_pt>50 && abs(GenPart_eta)<2.4 && {}'.format(prompt_gen_photons)
        rdf = rdf.Define('good_gen_photon',good_gen_photon) \
                 .Define('nga','Sum(good_gen_photon)')
        genPhotonBranches=['pt','eta','phi']
        for attr in genPhotonBranches:
            rdf=rdf.Define('g_'+attr,'nga > 0 ? GenPart_{}[good_gen_photon][0] : 0.'.format(attr))            
            branchList.append( 'g_'+attr )    
        rdf=rdf.Define('matched','int(na>0 && nga>0 && DeltaR(eta,g_eta,phi,g_phi)<0.1)') \
               .Define('matched_flav','na>0 ? Photon_genPartFlav[good_photon][0] : -99') 
        branchList += ['matched','matched_flav'] 
    
        #jets
        good_gen_jet  = 'GenJet_pt>15 && abs(GenJet_eta)<4.7'                           #kinematics
        good_gen_jet += ' && (nga==0 || crossClean(GenJet_eta,GenJet_phi,g_eta,g_phi))' #cross clean to photon candidate
        
        rdf = rdf.Define('good_gen_jet',good_gen_jet) \
                 .Define('good_tag_gen_jet','good_gen_jet && GenJet_pt>30') \
                 .Define('ngj',      'Sum(good_tag_gen_jet)') \
                 .Define('g_ptj1',   'ngj>0 ? GenJet_pt[good_tag_gen_jet][0] : -9999.') \
                 .Define('g_ptj2',   'ngj>1 ? GenJet_pt[good_tag_gen_jet][1] : -9999.') \
                 .Define('g_etaj1',  'ngj>0 ? GenJet_eta[good_tag_gen_jet][0] : -9999.') \
                 .Define('g_etaj2',  'ngj>1 ? GenJet_eta[good_tag_gen_jet][1] : -9999.') \
                 .Define('g_phij1',  'ngj>0 ? GenJet_phi[good_tag_gen_jet][0] : -9999.') \
                 .Define('g_phij2',  'ngj>1 ? GenJet_phi[good_tag_gen_jet][1] : -9999.') \
                 .Define('g_mj1',    'ngj>0 ? GenJet_mass[good_tag_gen_jet][0] : -9999.') \
                 .Define('g_mj2',    'ngj>1 ? GenJet_mass[good_tag_gen_jet][1] : -9999.') \
                 .Define('g_detajj', 'ngj>1 ? GenJet_eta[good_tag_gen_jet][0]-GenJet_eta[good_tag_gen_jet][1]: -9999.') \
                 .Define('g_mjj',    'ngj>1 ? GetPairInvariantMass(GenJet_pt[good_tag_gen_jet], GenJet_eta[good_tag_gen_jet], GenJet_phi[good_tag_gen_jet], GenJet_mass[good_tag_gen_jet], 0, 1): -9999.') \
                 .Define('g_balance','ngj>1 ? GetBalance(g_pt,g_phi,g_ptj1,g_phij1,g_ptj2,g_phij2) : -9999.')
        branchList += ['ngj',
                       'g_ptj1','g_ptj2','g_etaj1','g_etaj2','g_phij1','g_phij2','g_mj1','g_mj2',
                       'g_detajj','g_mjj','g_balance']

        rdf=rdf.Define('j1_matched','int(nj>0 && Jet_genJetIdx[good_tag_jet][0]>=0)') \
               .Define('j2_matched','int(nj>1 && Jet_genJetIdx[good_tag_jet][1]>=0)')
        branchList += ['j1_matched','j2_matched']

        #central jet info
        rdf = rdf.Define('central_gen_jet','centralJet(GenJet_pt[good_gen_jet],GenJet_eta[good_gen_jet],GenJet_phi[good_gen_jet],g_etaj1,g_phij1,g_etaj2,g_phij2)') \
                 .Define('g_ncenj','ngj>2 ? Sum(central_gen_jet) : 0.') \
                 .Define('g_htcen','g_ncenj>0 ? Sum(GenJet_pt[good_gen_jet][central_gen_jet]) : 0') \
                 .Define('g_ptj3', 'g_ncenj>0 ? GenJet_pt[good_gen_jet][central_gen_jet][0] : 0') \
                 .Define('g_etaj3','g_ncenj>0 ? GenJet_eta[good_gen_jet][central_gen_jet][0] : 0') \
                 .Define('g_phij3','g_ncenj>0 ? GenJet_phi[good_gen_jet][central_gen_jet][0] : 0') \
                 .Define('g_mj3',  'g_ncenj>0 ? GenJet_mass[good_gen_jet][central_gen_jet][0] : 0') \
                 .Define('j3_matched', 'int(nj>2 && ncenj>0 && Jet_genJetIdx[good_jet][central_jet][0]>=0)')
        branchList += [ 'g_ncenj','g_htcen','g_ptj3','g_etaj3','g_phij3','g_mj3','j3_matched' ]

        #pass generator level
        rdf=rdf.Define('pass_gen','int(nga>0 && ngj>1)')
        branchList += ['pass_gen']

    #apply skimming
    if isData:
        rdf=rdf.Filter('pass_skim>0','presel')
    else:
        rdf=rdf.Filter('pass_gen>0 || pass_skim>0','presel')
            
    #save snapshot
    snapshot_branches = ROOT.vector('string')()
    for b in branchList:
        snapshot_branches.push_back(b)
    rdf.Snapshot('data', out_file, snapshot_branches)
    
    #disable the implicit multi-threading if it has been used
    if local:
         ROOT.ROOT.DisableImplicitMT()
    
    #all done here
    return True


def runEWVJJSelectionTask(args):

    """runs the selection tasks on a given file/set of files"""
    
    try:
        
        #process files using the handle
        handle=args['handle']
        handle(args)
        
    except Exception as e:
        print('@ {} failed - better check locally'.format(args['era']))
        print(e)
        return False
    
    return True


def runEWVJJSelectionTaskList(tasks_to_run,base_args,ngroup=0):

    """
    loop over the tasks and submit the analysis in groups of ngroups and output the chunks to out_dir
    """
    
    #loop over tasks
    for task,files in tasks_to_run.items():
    
        args=copy.deepcopy(base_args)
    
        nfiles=len(files)

        #sub-divide in smaller groups
        if ngroup>0 and ngroup<=nfiles:
            for i in range(0, len(files),ngroup):
                args['era']='{}'.format(task,i)
                args['files']=files[i:i+ngroup]
                runEWVJJSelectionTask(args)
        else:
            args['era']=task
            args['files']=files
            runEWVJJSelectionTask(args)    