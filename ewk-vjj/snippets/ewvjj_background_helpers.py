import pandas as pd
import numpy as np
from snippets.plot_helpers import *

def fillControlHistograms(df,isData,eventWeightHandle=None,onlyThese=None):
    
    """
    defines the categorization masks which can be used
    if a weight handle is passed it will be called to update the event weights
    the event handle is expected to accept a category string and a dataframe as arguments
    """
        
    #prepare the selection masks to build different analysis categories
    masks={'lpt' : (df['Pass_lpt']) & (df['pt']>75) & (df['pt']<220),
           'lpt_v2' : (df['Pass_lpt']) & (df['pt']>75),
           'hpt' : (df['Pass_hpt']) & (df['pt']>220),
           'hpt_v2' : (df['Pass_hpt']) & (df['pt']>220), #this one is completed below
           'eb' : (df['isScEtaEB']>0),
           'r9' : (df['r9']>0.9),
           'loosenotkiso' : ((df['idcode'] & (1<<1))>0),
           'tightnotkiso' : ((df['idcode'] & (1<<7))>0),
           'tight' : (df['idcode']>=256),
           'v1j' : (df['ptj1']>50) & (df['puidj1']>0) & (df['puidj2']==0),
           'vbf' : (df['ptj1']>50) & (df['puidj1']>0) & (df['ptj2']>40) & (df['puidj2']>0) & (df['mjj']>200),
          }
    masks['vbfdeta3']=masks['vbf'] & (df['mjj']>300) & (np.abs(df['detajj'])>3)
    
    #complete high pT v2 masks
    if isData:
        masks['hpt_v2'] = masks['hpt_v2'] & ~df['Pass_lpt']
    else:
        masks['hpt_v2'] = masks['hpt_v2'] & ~(df['Pass_lpt'] & df['lpt_available'])
        
    masks['notightnotkiso']=~masks['tightnotkiso']
    masks['ee']=~masks['eb']
    
    #masks to use in each category
    categories={
        'lpt'          : ['lpt', 'eb', 'r9', 'tight', 'vbfdeta3'],
        'lpt_v2'       : ['lpt_v2','eb', 'r9', 'tight', 'vbfdeta3'],
        'lptv1j'       : ['lpt', 'eb', 'r9', 'tight', 'v1j'],
        'hpt'          : ['hpt', 'tight', 'vbf'],
        'hpt_v2'       : ['hpt_v2', 'tight', 'vbf'],
        'hptv1j'       : ['hpt', 'eb', 'r9', 'tight', 'v1j'],
        'lpt_loose'    : ['lpt','eb','r9','loosenotkiso','notightnotkiso','vbfdeta3'],
        'lpt_tight'    : ['lpt','eb','r9','tightnotkiso','vbfdeta3'],
        'hpt_eb_loose' : ['hpt','eb','loosenotkiso','notightnotkiso','vbf'],
        'hpt_eb_tight' : ['hpt','eb','tightnotkiso','vbf'],
        'hpt_ee_loose' : ['hpt','ee','loosenotkiso','notightnotkiso','vbf'],
        'hpt_ee_tight' : ['hpt','ee','tightnotkiso','vbf'],
    }
    categories['hpt_eb_deta3_loose'] = categories['hpt_eb_loose']+['vbfdeta3']
    categories['hpt_eb_deta3_tight'] = categories['hpt_eb_tight']+['vbfdeta3']
        
    #build the histograms for all the required categories
    histos={}
    for c,mlist in categories.items():
          
        if onlyThese and not c in onlyThese : continue
            
        #label for this category
        categoryLabel=''
        if c.find('lpt')==0 : categoryLabel  = 'Low $p_{T}$'
        if c.find('hpt')==0 : categoryLabel  = 'High $p_{T}$'
        if c.find('v1j')>0  : categoryLabel += '+1j'
        if '_v2' in c       : categoryLabel += ' v2'
        if '_loose' in c    : categoryLabel += ' loose-not tight'
        if '_tight' in c    : categoryLabel += ' tight'
        if '_ee' in c       : categoryLabel += ' EE'
        if '_eb' in c       : categoryLabel += ' EB'
        categoryLabel = r'%s'%categoryLabel
            
        #build final masks
        m = np.logical_and.reduce( [masks[x] for x in mlist ] )
        
        c_df=df[m]
        
        weights=None
        if not isData:
            if c.find('lpt')==0: weights=c_df['lpt_wgt']
            if c.find('hpt')==0: weights=c_df['hpt_wgt']
                    
        #update the weights if a eventWeightHandle has been passed
        if eventWeightHandle:
            kwargs={'category':c,'data':c_df}
            extra_weights = eventWeightHandle(**kwargs)
            if weights : weights=weights*extra_weights
            else : weights=extra_weights
              
        x_pt=c_df['pt']
        if x_pt.shape[0]==0 : continue
    
        #GENERIC CONTROL DISTRIBUTIONS
        histos[c+'_ntight'] = {'histo':countInHistogram(x=c_df['ntight'], w=weights, bins=[0.5,1.5,2.5,3.5]),
                              'label':categoryLabel,
                              'xlabel':r'Photon multiplicity'}
        histos[c+'_pt'] = {'histo':countInHistogram(x=x_pt, w=weights, bins=np.linspace(75,500,20)),
                           'label':categoryLabel,
                           'xlabel':r'Transverse momentum [GeV]'}
        histos[c+'_mjj'] = {'histo':countInHistogram(x=c_df['mjj'], w=weights, bins=np.linspace(300,2000,20)),
                            'label':categoryLabel,
                            'xlabel':r'Dijet invariant mass [GeV]'}
        c_df['absdetajj']=np.abs(c_df['detajj'])
        histos[c+'_detajj'] = {'histo':countInHistogram(x=c_df['absdetajj'], w=weights, bins=np.linspace(0,10,20)),
                               'label':categoryLabel,
                               'xlabel':r'$|\Delta\eta(jj)|$'}
        histos[c+'_r9'] = {'histo':countInHistogram(x=c_df['r9'], w=weights, bins=np.linspace(0.8,1,20)),
                             'label':categoryLabel,
                             'xlabel':r'$R_9$'}
        histos[c+'_sieie'] = {'histo':countInHistogram(x=c_df['sieie'], w=weights, bins=np.linspace(0,0.05,25)),
                                'label':categoryLabel,
                                'xlabel':r'$\sigma_{i\eta-i\eta}$'}
         
        #NONPROMPT PHOTONS isolation distributions the first bin will contain all events passing tight iso cf. 
        #https://twiki.cern.ch/twiki/bin/viewauth/CMS/CutBasedPhotonIdentificationRun2
        isobins=[0.,0.65,0.8,1.0,1.5,2.0,3.0,4.0,5.0,7.0,10.,20.,50.]
        if c.find('_ee_')>0:
            isobins=[0.,0.517,0.8,1.0,1.5,2.0,3.0,4.0,5.0,7.0,10.,20.,50.]
        isobins=np.array(isobins)
        histos[c+'_chiso'] = {'histo':countInHistogram(x=x_pt*c_df['pfRelIso03_chg'], w=weights, bins=isobins),
                                'label':categoryLabel,
                                'xlabel':r'$I_{CH} [GeV]$'}
        
        #do some equal stats bins for the projections (adapt the first and last bins)
        ptbins=[75,80,85,90,95,110,140,220]
        if 'hpt' in c: ptbins=[ 220,240,260,280,350,500,750,2000]
        if 'hpt_ee' in c: ptbins=[ 220,250,280,350,1000]
        if 'hpt_ee' in c or 'hpt_eb_deta3' in c: ptbins=[ 220,250,280,350,1000]
        ptbins=np.array(ptbins)
        try:
            histos[c+'_chisovspt'] = {'histo':countIn2DHistogram(x=x_pt, xbins=ptbins,
                                                                 y=x_pt*c_df['pfRelIso03_chg'], ybins=isobins,
                                                                 w=weights),
                                      'label':categoryLabel,
                                      'ylabel':r'$I_{CH} [GeV]$',
                                      'xlabel':r'Transverse momentum [GeV]'}
        except Exception as e:
            print('Unable to fill {}_chisovspt : {}'.format(c,e))
            pass
        
        #profile of the pT
        c_df['ptbin'] = np.digitize(x_pt, bins=ptbins)
        binned = c_df.groupby('ptbin')
        result = binned['pt'].agg(['mean', 'sem'])
        histos[c+'_ptprofile'] = { 'histo':(ptbins,result['mean'].values,result['sem'].values),
                                   'label':categoryLabel,
                                   'xlabel':r'Transverse momentum [GeV]',
                                   'ylabel':r'<Transverse momentum> [GeV]'}
        
        #PILEUP JETS individual jet-photon balance to check pileup contribution
        try:
            
            def deltaPhi(phi1,phi2):
                dphi=phi1-phi2
                dphi=np.where(dphi>2*np.pi,dphi-2*np.pi,dphi)
                dphi=np.where(dphi<0,dphi+2*np.pi,dphi)
                return dphi
                
            def getR(pt,phi,ptj,phij):
                dphi=deltaPhi(phi,phij)
                return 1.+(ptj/pt)*np.cos(dphi)           
            
            r_bins=np.linspace(-2,2,50)
            r1=getR(c_df['pt'],c_df['phi'],c_df['ptj1'],c_df['phij1'])
            r2=getR(c_df['pt'],c_df['phi'],c_df['ptj2'],c_df['phij2'])
            
            histos[c+'_r1vsr2'] = {'histo':countIn2DHistogram(x=r1, xbins=r_bins,
                                                              y=r2, ybins=r_bins,
                                                              w=weights),
                                    'label':categoryLabel,
                                    'xlabel':r'$R_{1}$',
                                    'ylabel':r'$R_{2}$'}
            histos[c+'_r1'] = {'histo':countInHistogram(x=r1,
                                                        w=weights,
                                                        bins=np.linspace(-2,2,50)),
                                'label':categoryLabel,
                                'xlabel':r'$R_{1}$'}
            histos[c+'_r2'] = {'histo':countInHistogram(x=r2,
                                                        w=weights,
                                                        bins=np.linspace(-2,2,50)),
                                'label':categoryLabel,
                                'xlabel':r'$R_{2}$'}
            
            #profile r1 and rclose versus detajj
            deta_bins=[3,3.5,4,4.5,5,6,7,8]
            if 'hpt' in c: deta_bins=[0,1,2,3,4,5,6,7,8]
            histos[c+'_r1vsdetajj'] = {'histo':countIn2DHistogram(x=c_df['absdetajj'], xbins=deta_bins,
                                                                  y=r1, ybins=np.linspace(-2,2,20),
                                                                  w=weights),
                                       'label':categoryLabel,
                                       'xlabel':r'$\Delta\eta$(jj)',
                                       'ylabel':r'$R_{1}$'}
            
            #profile of the detajj
            c_df['detabin'] = np.digitize(c_df['absdetajj'], bins=deta_bins)
            binned = c_df.groupby('detabin')
            result = binned['absdetajj'].agg(['mean', 'sem'])
            histos[c+'_detaprofile'] = { 'histo':(deta_bins,result['mean'].values,result['sem'].values),
                                         'label':categoryLabel,
                                         'xlabel':r'$\Delta\eta$(jj)',
                                         'ylabel':r'<$\Delta\eta$(jj)>'}
            
        except Exception as e:
            print('Unable to fill pileup jet-related histograms for {} : {}'.format(c,e))
            pass
    
        del c_df
        
    return histos,categories
