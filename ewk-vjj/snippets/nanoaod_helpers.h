#ifndef __nanoaod_helpers_h__ 
#define __nanoaod_helpers_h__ 
#include "ROOT/RVec.hxx"

using namespace ROOT::VecOps; 

using rvec_f = const RVec<float>;
using rvec_i = const RVec<int>;
using rvec_b = const RVec<bool>;

/**
    @short checks if a set of objects are isolated with respect to a reference in the eta-phi plane
*/
rvec_b crossClean(const rvec_f &eta,const rvec_f &phi, const float &eta_ref, const float &phi_ref,float cone=0.4)
{
    std::vector<bool> isIso;
    
    //loop over the first list of objects
    for(size_t i=0; i<eta.size(); i++) {
        
        //find the min. distance with respect to the second list of objects
        float minDR(9999.);
        isIso.push_back( DeltaR(eta[i],eta_ref,phi[i],phi_ref)>0.4 );
    }

    return rvec_b(isIso.begin(), isIso.end());
}

/**
    @short analyses the bits in the VID decision map and the value of the ID and returns a summary code
    [0] - pass looseID     
    [1] - pass looseID+iso (except tk)
    [2] - pass looseID+iso
    [3] - pass mediumID     
    [4] - pass mediumID+iso (except tk)
    [5] - pass mediumID+iso
    [6] - pass tightID
    [7] - pass tightID+iso (except tk)
    [8] - pass tightID+iso
    Notes for NanoAOD scheme of storing this information
    * vid: 0:fail, 1:loose, 2:medium, 3:tight
    * vid map: two bits per cut corresponding to: 0:fail, 1:loose, 2: medium, 3: tight
              [0:1] MinPtCut, [2:3] PhoSCEtaMultiRangeCut,  [4:5] PhoSingleTowerHadOverEmCut, [6:7] PhoFull5x5SigmaIEtaIEtaCut,
              [8:9] PhoGenericRhoPtScaledCut, [10:11] PhoGenericRhoPtScaledCut, [12:13] PhoGenericRhoPtScaledCut
    * cf. https://twiki.cern.ch/twiki/bin/view/CMS/CutBasedPhotonIdentificationRun2
*/
rvec_i getPhotonIDSummary(const rvec_i &vid, const rvec_i & vidmap) {
    
     std::vector<int> photonID(vid.size(),0);
     for(size_t i=0; i<vid.size(); i++) {
         
         //decode the id bits/codes
         bool isLoose(vid[i]>0);
         bool isMedium(vid[i]>1);
         bool isTight(vid[i]==3);
         int minPt( (vidmap[i] & 0x3) );
         int PhoSCEtaMultiRangeCut( (vidmap[i]>>2) & 0x3 );
         int PhoSingleTowerHadOverEmCut( (vidmap[i]>>4) & 0x3 );
         int PhoFull5x5SigmaIEtaIEtaCut( (vidmap[i]>>6) & 0x3 );
         int PhoGenericRhoPtScaledCut_ch( (vidmap[i]>>8) & 0x3 );
         int PhoGenericRhoPtScaledCut_nh( (vidmap[i]>>10) & 0x3 );
         int PhoGenericRhoPtScaledCut_ph( (vidmap[i]>>12) & 0x3 );
         
         //compose the code
         //loose bits
         if(minPt>0 && PhoSCEtaMultiRangeCut>0 && PhoSingleTowerHadOverEmCut>0 && PhoFull5x5SigmaIEtaIEtaCut>0) {
             photonID[i] |= (1<<0);        
             if(PhoGenericRhoPtScaledCut_nh>0 && PhoGenericRhoPtScaledCut_ph>0) {
                 photonID[i] |= (1<<1);        
             }
         }
         if(isLoose) photonID[i] |= (1<<2);

         //medium bits
         if(minPt>1 && PhoSCEtaMultiRangeCut>1 && PhoSingleTowerHadOverEmCut>1 && PhoFull5x5SigmaIEtaIEtaCut>1) {
             photonID[i] |= (1<<3);        
             if(PhoGenericRhoPtScaledCut_nh>1 && PhoGenericRhoPtScaledCut_ph>1) {
                 photonID[i] |= (1<<4);        
             }
         }
         if(isMedium) photonID[i] |= (1<<5);
         
         //tight bits
         if(minPt>2 && PhoSCEtaMultiRangeCut>2 && PhoSingleTowerHadOverEmCut>2 && PhoFull5x5SigmaIEtaIEtaCut>2) {
             photonID[i] |= (1<<6);        
             if(PhoGenericRhoPtScaledCut_nh>2 && PhoGenericRhoPtScaledCut_ph>2) {
                 photonID[i] |= (1<<7);        
             }
         }
         if(isTight) photonID[i] |= (1<<8);
     }
    
    return rvec_i(photonID.begin(), photonID.end());
}

/**
    @short checks if a collection of reconstruction particles are prompt final states    
*/
rvec_b isPromptFinalState(int pdgId,
                          const rvec_i &gen_pdgId, const rvec_i &gen_status, const rvec_i &gen_statusFlags
                          )
{
    std::vector<bool> result(gen_pdgId.size(),false);

    //for each reconstructed particle find best matching gen particle which is stable and prompt
    for(size_t j=0; j<gen_pdgId.size(); j++) {
        
        if(gen_pdgId[j]!=pdgId) continue;
        if(gen_status[j]!=1) continue;
        
        bool isPrompt( (gen_statusFlags[j]&0x1) );
        if(!isPrompt) continue;
        result[j]=true;
    }

    return rvec_b(result.begin(), result.end());
}

/**
    @short implements invariant mass of two objects 
*/
float MassAB(float pt1,float eta1,float phi1,float m1,
             float pt2,float eta2,float phi2,float m2) {

    ROOT::Math::PtEtaPhiMVector p1(pt1, eta1, phi1, m1);
    ROOT::Math::PtEtaPhiMVector p2(pt2, eta2, phi2, m2);
    return (p1 + p2).mass();
}


/**
    @short gets the invariant mass of a pair
*/
float GetPairInvariantMass(const rvec_f &pt, const rvec_f &eta, const rvec_f &phi, const rvec_f &m,size_t i=0, size_t j=1) {
    if(pt.size()<max(i,j)+1) return -9999.;
    return MassAB(pt[i], eta[i], phi[i], m[i],pt[j], eta[j], phi[j], m[j]);
}

/**
    @short computes the balance variable (A+B+C).Pt()
*/
float GetBalance(float pt1,float phi1,
                 float pt2,float phi2,
                 float pt3,float phi3) {
    TVector2 p1(0,0); p1.SetMagPhi(pt1,phi1);
    TVector2 p2(0,0); p2.SetMagPhi(pt2,phi2);
    TVector2 p3(0,0); p3.SetMagPhi(pt3,phi3);
    return (p1+p2+p3).Mod();
}


/**
    @short loops over the list of jets (excluding the first two assume as the tag jets) and checks if they are central
*/
rvec_b centralJet(const rvec_f &pt, const rvec_f &eta, const rvec_f &phi,
                  const float &etaj1, const float &phij1,
                  const float &etaj2, const float &phij2,
                  float epsEta=0.2)
{
    std::vector<bool> isCen(pt.size(),false);

    //require the tag jets to have been selected
    if(etaj1>-9000 || etaj2>-9000) {
    
        float minEta=TMath::Min(etaj1,etaj2);
        float maxEta=TMath::Max(etaj1,etaj2);
        
        //loop over the first list of jets
        for(size_t i=0; i<eta.size(); i++) {
        
            float dR=DeltaR(eta[i],etaj1,phi[i],phij1);
            if(dR<0.1) continue;
            dR=DeltaR(eta[i],etaj2,phi[i],phij2);
            if(dR<0.1) continue;
            if(pt[i]<15) continue;
            if(eta[i]<minEta+epsEta) continue;
            if(eta[i]>maxEta-epsEta) continue;
            isCen[i]=true;
        }
    }
        
    return rvec_b(isCen.begin(), isCen.end());
}



#endif
